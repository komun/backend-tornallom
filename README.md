# Backend Tornallom

## Introducción

El backend o panel de control de Tornallom es el motor detrás del mapa de [tornallom](https://tornallom.org), más info en su web.

Permite añadir los puntos de las diferentes iniciativas entrevistadas, añadir los detalles y la puntuación de los criterios.

## Mejoras frente al mapa original de MapAPam

- [x] Este backend soporta multi-idioma en los múltiples entidades de la base de datos(nombre de un punto, descripción, nombre de un criterio, etc.).
- [x] El frontend de [tornallom](https://tornallom.org) está basado en la plantilla Wordpress de [PamAPam-Frontend](https://gitlab.com/pamapam/frontend-dev) y ahora soporta multi-idioma.
- [x] Añadido selector que permite configurar si la puntuación de cada criterio es una suma de las sub-puntuaciones de cada pregunta (como el anterior backend de pamapam) o también si se quiere puntuar al criterio directamente.

## Futuro

- [ ] Añadir un frontend también a este código para poder ser autónomo.
- [ ] Poder embeber el mapa y directorio como iframes en otras páginas, incluido sus filtros.
- [ ] Sistema de backups automatizados y configurable por los admins desde el panel de administración del backend.
- [ ] Aumentar endpoints de la API para Related Entities(por sector y por proximidad)
- [ ] Etc.
***

## Agradecimientos
Agradecemos enormemente al equipo de [Tornallom](https://tornallom.org) por elegir a [Komun.org](https://komun.org) para el desarrollo de esta plataforma de software libre y a [PamAPam](https://pamapam.org) y [Jamgo](https://jamgo.coop/) por desarrollar el anterior backend para almacenar los datos y cuestionarios de cada una de las iniciativas que aparecen en el mapa.

## Licencia
Affero GPL-v3

## Estado del Proyecto
El desarrollo del proyecto sigue vivo y abiertos a recibir colaboraciones y peticiones de otros colectivos para replicar Tornallom para otros usos.
