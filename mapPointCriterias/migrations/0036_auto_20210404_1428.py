# Generated by Django 3.0 on 2021-04-04 12:28

import django.contrib.gis.db.models.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mapPointCriterias', '0035_auto_20210403_2014'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mappoint',
            name='location',
            field=django.contrib.gis.db.models.fields.PointField(blank=True, geography=True, null=True, srid=4326, verbose_name='Coordenades per al mapa'),
        ),
    ]
