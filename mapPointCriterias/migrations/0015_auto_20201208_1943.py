# Generated by Django 3.0 on 2020-12-08 19:43

from django.db import migrations, models
import django.db.models.deletion
import mapPointCriterias.models


class Migration(migrations.Migration):

    dependencies = [
        ('mapPointCriterias', '0014_auto_20201208_1549'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='criteria',
            options={'ordering': ['my_order'], 'verbose_name': 'Criteri', 'verbose_name_plural': 'Criteris'},
        ),
        migrations.AddField(
            model_name='criteria',
            name='my_order',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='pointlink',
            name='link_type',
            field=models.CharField(choices=[('WEBSITE', 'Lloc Web'), ('FACEBOOK', 'Facebook'), ('TWITTER', 'Twitter'), ('INSTAGRAM', 'Instagram'), ('PINTEREST', 'Pinterest'), ('MASTODON', 'Mastodon')], default='WEBSITE', max_length=10, verbose_name="Tipus d'enllaç"),
        ),
        migrations.CreateModel(
            name='EvaluationCriteriasForMapPoint',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('notes', mapPointCriterias.models.CharFieldWithTextarea(blank=True, max_length=1000, null=True, verbose_name='Notes')),
                ('dont_apply', models.BooleanField(verbose_name='No aplica')),
                ('points_1', models.CharField(choices=[('YES', 'Cumpleix'), ('HALF', 'A mitjes'), ('NO', 'No cumpleix')], max_length=10)),
                ('criteria', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='evaluations', to='mapPointCriterias.Criteria')),
                ('mappoint', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='evaluations', to='mapPointCriterias.MapPoint')),
            ],
        ),
    ]
