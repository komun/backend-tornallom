# Generated by Django 3.0 on 2021-04-03 17:21

from django.db import migrations, models
import django.db.models.deletion
import mapPointCriterias.models


class Migration(migrations.Migration):

    dependencies = [
        ('mapPointCriterias', '0031_auto_20210403_1518'),
    ]

    operations = [
        migrations.AlterField(
            model_name='categorytranslation',
            name='original',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='translations', to='mapPointCriterias.Category', verbose_name='Sector'),
        ),
        migrations.AlterField(
            model_name='criteriatranslation',
            name='original',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='translations', to='mapPointCriterias.Criteria', verbose_name='Criteria'),
        ),
        migrations.CreateModel(
            name='MapPointTranslation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=500, verbose_name='Nom traduit')),
                ('description', mapPointCriterias.models.CharFieldWithTextarea(blank=True, max_length=500, null=True, verbose_name='Descripció traduida')),
                ('language', models.CharField(choices=[('val', 'Valencià'), ('es', 'Castellano')], default='es', max_length=15, verbose_name='Idioma traducció')),
                ('original', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='translations', to='mapPointCriterias.MapPoint', verbose_name='MapPoint')),
            ],
        ),
    ]
