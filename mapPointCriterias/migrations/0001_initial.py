# Generated by Django 3.0 on 2020-11-13 21:04

from django.conf import settings
import django.contrib.auth.models
import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion
import mapPointCriterias.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0011_update_proxy_permissions'),
    ]

    operations = [
        migrations.CreateModel(
            name='BurocracyCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250, verbose_name='Nom')),
                ('codenumber', models.CharField(blank=True, max_length=5, null=True, unique=True, verbose_name='Codi')),
            ],
            options={
                'verbose_name': 'Sector CCAE',
                'verbose_name_plural': 'Sectors CCAE',
                'ordering': ['codenumber'],
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250, verbose_name='Nom')),
                ('description', mapPointCriterias.models.CharFieldWithTextarea(blank=True, max_length=500, null=True, verbose_name='Descripció')),
                ('icon', models.ImageField(default='category_icons/default.png', help_text='Dimensions minimes: 40 x 40px.', upload_to='category_icons', verbose_name='Icona')),
                ('mapicon', models.ImageField(default='category_mapicons/default.png', help_text='Dimensions minimes: 40 x 40px.', upload_to='category_mapicons', verbose_name='Icona del mapa')),
                ('parent', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='mapPointCriterias.Category', verbose_name='Subcategoria de:')),
            ],
            options={
                'verbose_name': 'Categoria',
                'verbose_name_plural': 'Categories',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Community',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250, verbose_name='Nom')),
                ('description', mapPointCriterias.models.CharFieldWithTextarea(blank=True, max_length=500, null=True, verbose_name='Descripció')),
            ],
            options={
                'verbose_name': 'Comunitat',
                'verbose_name_plural': 'Comunitats',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Criteria',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250, verbose_name='Nom')),
                ('description', mapPointCriterias.models.CharFieldWithTextarea(blank=True, max_length=1000, null=True, verbose_name='Descripció')),
                ('question', models.CharField(max_length=1000, verbose_name='Pregunta')),
                ('answer_1', mapPointCriterias.models.CharFieldWithTextarea(blank=True, max_length=1000, null=True, verbose_name='Resposta 1')),
                ('answer_2', mapPointCriterias.models.CharFieldWithTextarea(blank=True, max_length=1000, null=True, verbose_name='Resposta 2')),
                ('answer_3', mapPointCriterias.models.CharFieldWithTextarea(blank=True, max_length=1000, null=True, verbose_name='Resposta 3')),
                ('answer_4', mapPointCriterias.models.CharFieldWithTextarea(blank=True, max_length=1000, null=True, verbose_name='Resposta 4')),
                ('answer_5', mapPointCriterias.models.CharFieldWithTextarea(blank=True, max_length=1000, null=True, verbose_name='Resposta 5')),
                ('answer_6', mapPointCriterias.models.CharFieldWithTextarea(blank=True, max_length=1000, null=True, verbose_name='Resposta 6')),
                ('icon', models.ImageField(default='images/criteria_icons/default.png', help_text='Dimensió mínima: 40 x 40px.', upload_to='images/criteria_icons', verbose_name='Icona')),
            ],
            options={
                'verbose_name': 'Criteri',
                'verbose_name_plural': 'Criteris',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='LandLevel_1',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250, verbose_name='Nom')),
            ],
            options={
                'verbose_name': 'Provincia',
                'verbose_name_plural': 'Provincies',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='LandLevel_2',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250, verbose_name='Nom')),
                ('upper_level', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mapPointCriterias.LandLevel_1', verbose_name='Provincia')),
            ],
            options={
                'verbose_name': 'Comarca',
                'verbose_name_plural': 'Comarques',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='LandLevel_3',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250, verbose_name='Nom')),
                ('upper_level', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mapPointCriterias.LandLevel_2', verbose_name='Comarca')),
            ],
            options={
                'verbose_name': 'Municipi',
                'verbose_name_plural': 'Municipis',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Language',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20, verbose_name='Nom')),
                ('i10n_code', models.CharField(blank=True, max_length=20, null=True, verbose_name='Codi i10n')),
            ],
            options={
                'verbose_name': 'Idioma',
                'verbose_name_plural': 'Idiomes',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='LegalForm',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250, verbose_name='Nom')),
            ],
            options={
                'verbose_name': 'Forma Jurídica',
                'verbose_name_plural': 'Formes Jurídiques',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Network',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250, verbose_name='Nom')),
            ],
            options={
                'verbose_name': 'Xarxa',
                'verbose_name_plural': 'Xarxes',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='ParticipantRole',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250, verbose_name='Nom')),
                ('description', mapPointCriterias.models.CharFieldWithTextarea(blank=True, max_length=500, null=True, verbose_name='Descripció')),
            ],
            options={
                'verbose_name': 'Rol persones implicades',
                'verbose_name_plural': 'Rols persones implicades',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='PointStatus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30, verbose_name='Nom')),
                ('codename', models.CharField(max_length=20, unique=True, verbose_name='Codename')),
                ('type', models.CharField(choices=[('DRAFT', 'DRAFT'), ('REVIEW_PENDING', 'REVIEW_PENDING'), ('INCOMPLETE', 'INCOMPLETE'), ('PUBLISHED', 'PUBLISHED'), ('UNPUBLISHED', 'UNPUBLISHED'), ('INITIATIVE', 'INITIATIVE'), ('PROPOSED', 'PROPOSED'), ('ARCHIVED', 'ARCHIVED')], max_length=15, verbose_name='Tipus de Estat')),
            ],
            options={
                'verbose_name': 'Estat',
                'verbose_name_plural': 'Estats',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Role',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250, verbose_name='Nom')),
                ('description', mapPointCriterias.models.CharFieldWithTextarea(blank=True, max_length=500, null=True, verbose_name='Descripció')),
            ],
            options={
                'verbose_name': 'Rol',
                'verbose_name_plural': 'Rols',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('language', models.CharField(choices=[('ca-valencia', 'Valencià'), ('es-ES', 'Castellano')], default='ca-valencia', help_text="El idioma de l'interfaç.", max_length=15, verbose_name='Idioma interfaç')),
                ('notes', mapPointCriterias.models.CharFieldWithTextarea(blank=True, max_length=500, null=True, verbose_name='Notes')),
                ('photograph', models.ImageField(blank=True, default='user_profiles/default.png', help_text='Dimensiones mínimas recomendadas: 200 x 100px.', null=True, upload_to='user_profiles', verbose_name='Fotografia')),
                ('twitter', models.CharField(blank=True, max_length=100, null=True, verbose_name='Twitter')),
                ('facebook', models.CharField(blank=True, max_length=100, null=True, verbose_name='Facebook')),
                ('consent_date', models.DateField(blank=True, null=True, verbose_name='Consent política de dades')),
                ('community', models.ManyToManyField(blank=True, to='mapPointCriterias.Community', verbose_name='Comunitats')),
            ],
            options={
                'verbose_name': 'Perfil usuari@',
                'verbose_name_plural': 'Perfils usuari@',
                'ordering': ['user'],
            },
            bases=('auth.user',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('point_types', models.CharField(blank=True, choices=[('OWN', 'Propis'), ('OTHERS', "D'altres")], max_length=6, null=True, verbose_name='Quins punts?')),
                ('trigger_actions', mapPointCriterias.models.ChoiceArrayField(base_field=models.CharField(choices=[('CREATED', 'Creat'), ('EDIT_BODY', 'Contingut Modificat'), ('EDIT_STATUS', 'Estat Modificat'), ('DELETED', 'Eliminat')], max_length=12, verbose_name='Quan un punt es:'), default=list, size=None)),
                ('title', models.CharField(max_length=250, verbose_name='Assumpte de la notificació')),
                ('body', models.TextField(max_length=250, verbose_name='Text de la notificació')),
                ('status_change', models.ManyToManyField(to='mapPointCriterias.PointStatus', verbose_name='Si el estat del punt cambia a:')),
            ],
            options={
                'verbose_name': 'Notificació',
                'verbose_name_plural': 'Notificacions',
                'ordering': ['title'],
            },
        ),
        migrations.CreateModel(
            name='MapPoint',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250, verbose_name='Nom')),
                ('burocracy_number', models.CharField(blank=True, max_length=20, null=True, verbose_name='NIF/CIF')),
                ('description', models.TextField(blank=True, max_length=1000, null=True, verbose_name='Descripció')),
                ('year', models.CharField(blank=True, max_length=500, null=True, verbose_name='Any de Fundació')),
                ('picture_1', models.ImageField(help_text='Dimensiones mínimas recomendadas: 200 x 100px.', upload_to='point_images', verbose_name='Fotografia 1')),
                ('picture_2', models.ImageField(help_text='Dimensiones mínimas recomendadas: 200 x 100px.', upload_to='point_images', verbose_name='Fotografia 2')),
                ('postal_code', models.CharField(blank=True, max_length=10, null=True, verbose_name='Codi Postal')),
                ('location', django.contrib.gis.db.models.fields.PointField(blank=True, null=True, srid=4326)),
                ('address', models.CharField(blank=True, max_length=100, null=True)),
                ('timetable', models.CharField(blank=True, max_length=250, null=True, verbose_name='Horari')),
                ('language', models.CharField(choices=[('ca-valencia', 'Valencià'), ('es-ES', 'Castellano')], default='ca-valencia', help_text="El idioma de l'interfaç.", max_length=15, verbose_name='Idioma interfaç')),
                ('published_date', models.DateTimeField(blank=True, null=True, verbose_name='Data Publicació')),
                ('contact_person', models.CharField(blank=True, max_length=250, null=True, verbose_name='Persona de contacte')),
                ('email', models.EmailField(blank=True, max_length=254, null=True, verbose_name='Correu electrònic')),
                ('extra_description', mapPointCriterias.models.CharFieldWithTextarea(blank=True, max_length=500, null=True, verbose_name='Qué hi pots trobar?')),
                ('invoicing', models.CharField(blank=True, max_length=250, null=True, verbose_name='Facturació anual')),
                ('other_networks', models.CharField(blank=True, max_length=250, null=True, verbose_name='Altres xarxes')),
                ('social_approval', models.CharField(blank=True, choices=[('YES', 'Si'), ('NO', 'No'), ('NOT_APPLY', 'No Aplica')], max_length=10, null=True, verbose_name='Aprovat en Balanç Social?')),
                ('burocracy_category', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='mapPointCriterias.BurocracyCategory', verbose_name='Sector CCAE')),
                ('editor', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='mapPointCriterias.UserProfile', verbose_name='Editor Responsable')),
                ('landlevel_1', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='mapPointCriterias.LandLevel_1', verbose_name='Provincia')),
                ('landlevel_2', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='mapPointCriterias.LandLevel_2', verbose_name='Comarca')),
                ('landlevel_3', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='mapPointCriterias.LandLevel_3', verbose_name='Municipi')),
                ('legal_form', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='mapPointCriterias.LegalForm', verbose_name='Forma Jurídica')),
                ('main_category', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='points', to='mapPointCriterias.Category', verbose_name='Sector principal')),
                ('networks', models.ManyToManyField(to='mapPointCriterias.Network', verbose_name="Xarxes de l'Economia Social")),
                ('other_categories', models.ManyToManyField(to='mapPointCriterias.Category', verbose_name='Sectors secondaris')),
                ('status', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='mapPointCriterias.PointStatus', verbose_name='Estat')),
            ],
            options={
                'verbose_name': 'Punt',
                'verbose_name_plural': 'Punts',
                'ordering': ['published_date'],
            },
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.CharField(max_length=250, verbose_name='Nom')),
                ('published_date', models.DateTimeField(verbose_name='Data Publicació')),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='mapPointCriterias.UserProfile', verbose_name='Autor/a')),
                ('mappoint', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='point_comments', to='mapPointCriterias.MapPoint', verbose_name='Punt')),
            ],
            options={
                'verbose_name': 'Comentari',
                'verbose_name_plural': 'Comentaris',
                'ordering': ['published_date'],
            },
        ),
    ]
