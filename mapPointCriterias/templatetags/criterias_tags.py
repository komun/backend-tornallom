from django import template
register = template.Library()

from mapPointCriterias.models import Criteria, ParticipantRole, EvaluationCriteriasForMapPoint, ParticipantRoleForMapPoint
from django.core.exceptions import ObjectDoesNotExist

@register.simple_tag
def get_all_criterias(queryset, **filters):
    if not filters:
        raise template.TemplateSyntaxError('`get_all_criterias` tag requires filters.')
    return Criteria.objects.all().order_by('my_order')#filter(**filters).first()

@register.simple_tag
def get_criteria_answers(point, criteria,  **filters):
    try:
        return EvaluationCriteriasForMapPoint.objects.get(criteria=criteria, mappoint=point)
    except ObjectDoesNotExist:
        return None

@register.simple_tag
def get_all_roles(queryset, **filters):
    return ParticipantRole.objects.all().order_by('my_order')

@register.simple_tag
def get_role_answers(point, role,  **filters):
    try:
        answers = ParticipantRoleForMapPoint.objects.get(role=role, mappoint=point)
    except ObjectDoesNotExist:
        answers = None

    return answers
