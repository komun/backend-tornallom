from django.contrib.auth.forms import UserCreationForm
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django import forms
from .models import (
    UserProfile,
    Community,
    Comment,
    MapPoint,
    CategoryTranslation,
    ParticipantRoleForMapPoint,
    ParticipantRole,
    Criteria,
    EvaluationCriteriasForMapPoint,
    EvaluationResultType,
)
from django.core.exceptions import ValidationError


class CustomCommentForm(forms.ModelForm):
    text = forms.CharField(
        label="Comentari",
        min_length=4,
        widget=forms.Textarea(attrs={"rows": 4, "cols": 100}),
    )

    class Meta:
        model = Comment
        exclude = ["mappoint", "author", "published_date"]  #


class CustomTranslationForm(forms.ModelForm):
    name = forms.CharField(
        min_length=4,
        widget=forms.Textarea(attrs={"rows": 1, "cols": 30}),
    )
    description = forms.CharField(
        min_length=4,
        widget=forms.Textarea(attrs={"rows": 3, "cols": 50}),
    )

    class Meta:
        model = CategoryTranslation
        exclude = ["__str__", "__unicode__"]


class CustomUserCreationForm(forms.ModelForm):
    # username = forms.CharField(label="Enter Username", min_length=4, max_length=20)
    email = forms.EmailField(
        label=_("Email"),
        required=True,
        max_length=30,
        widget=forms.TextInput(attrs={"style": "min-width: 12em"}),
    )
    password1 = forms.CharField(
        label="Cambiar contrasenya", widget=forms.PasswordInput, required=False
    )
    password2 = forms.CharField(
        label="Confirmar contrasenya", widget=forms.PasswordInput, required=False
    )
    # community = forms.ModelMultipleChoiceField( queryset=Community.objects.all(), required=False)

    class Meta:
        model = UserProfile
        exclude = []

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(CustomUserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

    def clean_username(self):
        username = self.cleaned_data["username"].lower()
        r = User.objects.filter(username=username)
        if r.count():
            raise ValidationError("Username already exists")
        return username

    def clean_email(self):
        email = self.cleaned_data["email"].lower()
        r = User.objects.filter(email=email)
        if r.count():
            raise ValidationError("Email already exists")
        return email

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")

        if password1 and password2 and password1 != password2:
            raise ValidationError("Password don't match")

        return password2

    # def save(self, commit=True):
    #     user = User.objects.create_user(
    #         self.cleaned_data['username'],
    #         self.cleaned_data['email'],
    #         self.cleaned_data['password1']
    #     )
    #     return user


def save_role_participants(request, obj):
    roles = ParticipantRole.objects.all().order_by("my_order")
    for role in roles:
        my_order = role.my_order
        try:
            women = int(request.POST[f"participants-women-{my_order}"])
        except:
            women = 0
        try:
            men = int(request.POST[f"participants-men-{my_order}"])
        except:
            men = 0
        try:
            others = int(request.POST[f"participants-others-{my_order}"])
        except:
            others = 0

        print("Role", my_order, role.name, women, men, others)

        role_answers, _ = ParticipantRoleForMapPoint.objects.get_or_create(
            role=role, mappoint=obj
        )
        role_answers.women = women
        role_answers.men = men
        role_answers.others = others
        role_answers.save()


def save_criterias(request, obj):
    criterias = Criteria.objects.all().order_by("my_order")
    for cri in criterias:
        my_order = cri.my_order
        criteria_answers, _ = EvaluationCriteriasForMapPoint.objects.get_or_create(
            criteria=cri, mappoint=obj
        )
        if f"dont_apply_criteria-{my_order}" in request.POST:
            criteria_answers.dont_apply = True
        else:
            criteria_answers.dont_apply = False
        criteria_answers.notes = request.POST[f"criteria_comment-{my_order}"]
        value = request.POST[f"criteria_points-{my_order}"]
        criteria_answers.overal_points = value if value != "none" else None

        value = request.POST[f"criteria_question-{my_order}-0"]
        criteria_answers.points_0 = value if value != "none" else None
        value = request.POST[f"criteria_question-{my_order}-1"]
        criteria_answers.points_1 = value if value != "none" else None
        value = request.POST[f"criteria_question-{my_order}-2"]
        criteria_answers.points_2 = value if value != "none" else None
        value = request.POST[f"criteria_question-{my_order}-3"]
        criteria_answers.points_3 = value if value != "none" else None
        value = request.POST[f"criteria_question-{my_order}-4"]
        criteria_answers.points_4 = value if value != "none" else None
        value = request.POST[f"criteria_question-{my_order}-5"]
        criteria_answers.points_5 = value if value != "none" else None
        criteria_answers.save()
