# -*- coding: utf-8 -*-

from django.db import models
from django import forms
from django.utils import timezone
from django.utils.html import mark_safe
import datetime
from time import strftime
from django.contrib.sessions.models import Session
from django.core.validators import RegexValidator
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.template.defaultfilters import date as _date
from django.contrib.auth.models import User
import os
from django.utils import timezone
from django.contrib.gis.db import models as gis_models
from django.template.defaultfilters import slugify
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

# from django_project.settings import BASE_DIR
from django.contrib.postgres.fields import ArrayField


class CharFieldWithTextarea(models.CharField):
    def formfield(self, **kwargs):
        kwargs.update({"widget": forms.Textarea(attrs={"rows": 3, "cols": 100})})
        return super(CharFieldWithTextarea, self).formfield(**kwargs)


class ChoiceArrayField(ArrayField):
    """
    A field that allows us to store an array of choices.

    Uses Django 1.9's postgres ArrayField
    and a MultipleChoiceField for its formfield.

    Usage:

        choices = ChoiceArrayField(models.CharField(max_length=...,
                                                    choices=(...,)),
                                   default=[...])
    """

    def formfield(self, **kwargs):
        defaults = {
            "form_class": forms.MultipleChoiceField,
            "choices": self.base_field.choices,
            "label": self.base_field.verbose_name,
        }
        defaults.update(kwargs)
        # Skip our parent's formfield implementation completely as we don't
        # care for it.
        # pylint:disable=bad-super-call
        return super(ArrayField, self).formfield(**defaults)

    #
    # def __init(self, label):
    #     self.formfield['label'] = label


DRAFT = "DRAFT"
REVIEW_PENDING = "REVIEW_PENDING"
INCOMPLETE = "INCOMPLETE"
PUBLISHED = "PUBLISHED"
UNPUBLISHED = "UNPUBLISHED"
UPDATE_PENDING = "UPDATE_PENDING"
INITIATIVE = "INITIATIVE"
PROPOSED = "PROPOSED"
ARCHIVED = "ARCHIVED"

POINT_TYPES = (
    (DRAFT, DRAFT),
    (REVIEW_PENDING, REVIEW_PENDING),
    (INCOMPLETE, INCOMPLETE),
    (PUBLISHED, PUBLISHED),
    (UNPUBLISHED, UNPUBLISHED),
    (INITIATIVE, INITIATIVE),
    (PROPOSED, PROPOSED),
    (ARCHIVED, ARCHIVED),
)


class PointStatusChoices(models.TextChoices):
    DRAFT = "DRAFT", "Esborrany"
    REVIEW_PENDING = "REVIEW_PENDING", "Pendent de revisió"
    PUBLISHED = "PUBLISHED", "Publicat"
    ARCHIVED = "ARCHIVED", "Arxivat"


# Status of Point of Interest
# class PointStatus(models.Model):
#     name = models.CharField(_("Nom"), max_length=30)
#     codename = models.CharField(
#         _("Codename"), max_length=20, unique=True
#     )  # regexp username
#     type = models.CharField(_("Tipus de Estat"), max_length=15, choices=POINT_TYPES)
#     # next = models.ForeignKey('PointStatus', verbose_name=_(u'Següent estat:'), null=True, blank=True, on_delete=models.DO_NOTHING)
#     # description = CharFieldWithTextarea(_(u'Descripció'), max_length=500, null=True, blank=True)
#
#     class Meta:
#         verbose_name = _("Estat")
#         verbose_name_plural = _("Estats")
#         ordering = ["name"]
#
#     def __str__(self):
#         return self.name


class Role(models.Model):
    name = models.CharField(
        _("Nom"), max_length=250
    )  # que sea codename como usuario, regex?
    description = CharFieldWithTextarea(
        _("Descripció"), max_length=500, null=True, blank=True
    )

    class Meta:
        verbose_name = _("Rol")
        verbose_name_plural = _("Rols")
        ordering = ["name"]

    def __str__(self):
        return self.name


class Community(models.Model):
    name = models.CharField(_("Nom"), max_length=250)
    description = CharFieldWithTextarea(
        _("Descripció"), max_length=500, null=True, blank=True
    )

    class Meta:
        verbose_name = _("Comunitat")
        verbose_name_plural = _("Comunitats")
        ordering = ["name"]

    def __str__(self):
        return self.name


# class Language(models.Model):
#     name = models.CharField(_(u'Nom'), max_length=20)
#     i10n_code = models.CharField(_(u'Codi i10n'), max_length=20, null=True, blank=True)
#
#     class Meta:
#         verbose_name=_(u'Idioma')
#         verbose_name_plural = _(u'Idiomes')
#         ordering=['name']
#
#     def __str__(self):
#         return self.name


class UserProfile(models.Model):
    user = models.OneToOneField(
        User,
        primary_key=True,
        on_delete=models.CASCADE,
    )
    language = models.CharField(
        _("Idioma interfaç"),
        max_length=15,
        choices=settings.LANGUAGE_CHOICES,
        default=settings.DEFAULT_LANGUAGE,
        help_text=_("El idioma de l'interfaç."),
    )
    notes = CharFieldWithTextarea(_("Notes"), max_length=500, null=True, blank=True)
    photograph = models.ImageField(
        upload_to="user_profiles",
        null=True,
        blank=True,
        verbose_name=_("Fotografia"),
        help_text=_("Dimensiones mínimas recomendadas: 200 x 100px."),
        default="user_profiles/default.png",
    )
    community = models.ManyToManyField(
        Community, verbose_name=_("Comunitats"), blank=True
    )
    consent_date = models.DateField(
        _("Consent política de dades"), null=True, blank=True
    )
    # role = models.ManyToManyField(Role, verbose_name=_(u'Roles'))
    # ciudad = models.ForeignKey('Poblacion', verbose_name=_(u'Población'), null=True, blank=True)
    # phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message=_(u"El número de teléfono tiene que tener el formato: '+1234567890' y una longitud máxima de 15 caracteres."))

    # def usuarios(self):
    #     return ";\n".join([u.username for u in self.users.all()])
    # usuarios.short_description= _(u'Usuarios')
    class Meta:
        verbose_name = _("Perfil usuari@")
        verbose_name_plural = _("Perfils usuari@")
        ordering = ["user"]

    def __str__(self):
        return "<usr:{}>".format(self.user.username)

    #
    # def save_m2m(self):
    #     self.instance.user_set.set(self.cleaned_data['users'])
    #

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            print("create_user_profile", instance)
            up = UserProfile.objects.create(user=instance)
            instance.userprofile = up
            instance.save()

    #
    # @receiver(post_save, sender=User)
    # def save_user_profile(sender, instance, **kwargs):
    #     print('save_user_profile', instance)
    #     instance.userprofile = self
    #     instance.userprofile.save()


class Notification(models.Model):
    OWN = "OWN"
    OTHERS = "OTHERS"

    POI_TYPE = (
        (OWN, _("Propis")),
        (OTHERS, _("D'altres")),
    )
    CREATED = "CREATED"
    EDIT_BODY = "EDIT_BODY"
    EDIT_STATUS = "EDIT_STATUS"
    DELETED = "DELETED"

    TRIGGER_ACTIONS = (
        (CREATED, _("Creat")),
        (EDIT_BODY, _("Contingut Modificat")),
        (EDIT_STATUS, _("Estat Modificat")),
        (DELETED, _("Eliminat")),
    )
    # Cambiar a Group role = models.ForeignKey('Role', verbose_name=_(u'Rol'), on_delete=models.DO_NOTHING)
    point_types = models.CharField(
        _("Quins punts?"), max_length=6, choices=POI_TYPE, null=True, blank=True
    )
    # trigger_action = ManyToMany choices ??
    trigger_actions = ChoiceArrayField(
        base_field=models.CharField(
            verbose_name=_("Quan un punt es:"), max_length=12, choices=TRIGGER_ACTIONS
        ),
        default=list,
    )
    # status_change = models.ManyToManyField(Trigger, verbose_name=_(u'Quan un punt es:'))
    # status_change = models.ManyToManyField(
    #     PointStatus, verbose_name=_("Si el estat del punt cambia a:")
    # )
    status = models.CharField(
        _("Estat del formulari"),
        max_length=15,
        default=PointStatusChoices.DRAFT,
        choices=PointStatusChoices.choices,
        null=True,
    )
    title = models.CharField(_("Assumpte de la notificació"), max_length=250)
    body = models.TextField(_("Text de la notificació"), max_length=250)

    class Meta:
        verbose_name = _("Notificació")
        verbose_name_plural = _("Notificacions")
        ordering = ["title"]

    def __str__(self):
        return self.title


class LandLevel_1(models.Model):
    name = models.CharField(_("Nom"), max_length=250)

    class Meta:
        verbose_name = _("Provincia")
        verbose_name_plural = _("Provincies")
        ordering = ["name"]

    def __str__(self):
        return self.name


class LandLevel_2(models.Model):
    name = models.CharField(_("Nom"), max_length=250)
    parent = models.ForeignKey(
        LandLevel_1, verbose_name=_("Provincia"), on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = _("Comarca")
        verbose_name_plural = _("Comarques")
        ordering = ["name"]

    def __str__(self):
        return self.name


class LandLevel_3(models.Model):
    name = models.CharField(_("Nom"), max_length=250)
    parent = models.ForeignKey(
        "LandLevel_2", verbose_name=_("Comarca"), on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = _("Municipi")
        verbose_name_plural = _("Municipis")
        ordering = ["name"]

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(_("Nom"), max_length=250)
    # api_id = models.SlugField(max_length = 200)
    parent = models.ForeignKey(
        "Category",
        verbose_name=_("Subcategoria de:"),
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    description = CharFieldWithTextarea(
        _("Descripció"), max_length=1000, null=True, blank=True
    )
    icon = models.ImageField(
        upload_to="category_icons",
        verbose_name=_("Icona"),
        help_text=_("Dimensions minimes: 40 x 40px."),
        default="category_icons/default.png",
    )
    mapicon = models.ImageField(
        upload_to="category_mapicons",
        verbose_name=_("Icona del mapa"),
        help_text=_("Dimensions minimes: 40 x 40px."),
        default="category_mapicons/default.png",
    )

    class Meta:
        verbose_name = _("Sector")
        verbose_name_plural = _("Sectors")
        ordering = ["name"]

    def __str__(self):
        return self.name


class CategoryTranslation(models.Model):
    name = models.CharField(_("Nom traduit"), max_length=250)
    description = CharFieldWithTextarea(
        _("Descripció traduida"), max_length=1000, null=True, blank=True
    )
    language = models.CharField(
        _("Idioma traducció"),
        max_length=15,
        choices=settings.LANGUAGE_CHOICES,
        default=settings.LANGUAGE_2,
    )
    original = models.ForeignKey(
        "Category",
        verbose_name=_("Sector"),
        on_delete=models.CASCADE,
        related_name="translations",
    )

    class Meta:
        verbose_name = _("Category Translation")
        verbose_name_plural = _("Category Translations")
        ordering = ["name"]

    def __str__(self):
        return "{}:{}".format(self.language, self.name)


class BurocracyCategory(models.Model):
    name = models.CharField(_("Nom"), max_length=250)
    codenumber = models.CharField(
        _("Codi"), max_length=5, unique=True, blank=True, null=True
    )  # regexp username

    class Meta:
        verbose_name = _("Sector CCAE")
        verbose_name_plural = _("Sectors CCAE")
        ordering = ["codenumber"]

    def __str__(self):
        return "{}:{}".format(self.codenumber, self.name)


class LegalForm(models.Model):
    name = models.CharField(_("Nom"), max_length=250)

    class Meta:
        verbose_name = _("Forma Jurídica")
        verbose_name_plural = _("Formes Jurídiques")
        ordering = ["name"]

    def __str__(self):
        return self.name


class Network(models.Model):
    name = models.CharField(_("Nom"), max_length=250)

    class Meta:
        verbose_name = _("Xarxa")
        verbose_name_plural = _("Xarxes")
        ordering = ["name"]

    def __str__(self):
        return self.name


class ParticipantRole(models.Model):
    name = models.CharField(_("Nom"), max_length=250)
    description = CharFieldWithTextarea(
        _("Descripció"), max_length=500, null=True, blank=True
    )
    my_order = models.PositiveIntegerField(
        _("Ordre"), default=0, blank=False, null=False
    )

    class Meta:
        verbose_name = _("Rol persones implicades")
        verbose_name_plural = _("Rols persones implicades")
        ordering = ["my_order"]

    def __str__(self):
        return self.name  # f"Role {self.my_order}"


class ParticipantRoleForMapPoint(models.Model):
    women = models.IntegerField(null=False, blank=False, default=0)
    men = models.IntegerField(null=False, blank=False, default=0)
    others = models.IntegerField(null=False, blank=False, default=0)

    mappoint = models.ForeignKey(
        "MapPoint", on_delete=models.CASCADE, related_name="roles"
    )
    role = models.ForeignKey(
        "ParticipantRole", on_delete=models.CASCADE, related_name="answers"
    )

    @property
    def total_people(self):
        return women + men + others

    def __str__(self):
        return f"Role {self.role.my_order}:{self.women},{self.men},{self.others}"


class EvaluationResultType(models.TextChoices):
    SUM_ANSWERS = "SUM_ANSWERS", _("Suma de respostes")
    OVERALL = "OVERALL", _("Sobre el criteri")


# class EvaluationPoints(models.TextChoices):
#     YES = 'YES', _(u'Cumpleix')
#     HALF = 'HALF', _(u'A mitjes')
#     NO = 'NO', _(u'No cumpleix')


class EvaluationCriteriasForMapPoint(models.Model):
    notes = CharFieldWithTextarea(_("Notes"), max_length=3000, null=True, blank=True)
    dont_apply = models.BooleanField(_("No aplica"), null=True)
    # cambiar esto por relacion de 1:N puntos
    points_0 = models.FloatField(null=True, blank=True)
    points_1 = models.FloatField(null=True, blank=True)
    points_2 = models.FloatField(null=True, blank=True)
    points_3 = models.FloatField(null=True, blank=True)
    points_4 = models.FloatField(null=True, blank=True)
    points_5 = models.FloatField(null=True, blank=True)
    overal_points = models.FloatField(null=True, blank=True)
    mappoint = models.ForeignKey(
        "MapPoint", on_delete=models.CASCADE, related_name="evaluations"
    )
    criteria = models.ForeignKey(
        "Criteria", on_delete=models.CASCADE, related_name="evaluations"
    )

    @property
    def is_ok(self):
        if self.total_points:
            return self.total_points > 0
        else:
            return False

    @property
    def total_points(self):
        if self.mappoint.evaluation_result_type == EvaluationResultType.OVERALL:
            return self.overal_points
        else:
            total = 0
            if self.points_0:
                total += self.points_0
            if self.points_1:
                total += self.points_1
            if self.points_2:
                total += self.points_2
            if self.points_3:
                total += self.points_3
            if self.points_4:
                total += self.points_4
            if self.points_5:
                total += self.points_5
            if total > 5:
                total = 5
            return total


class Criteria(models.Model):
    name = models.CharField(_("Nom"), max_length=250)
    slug = models.SlugField(
        _("Nom sense espais"), max_length=200
    )  # autoguardar en Save.
    description = CharFieldWithTextarea(
        _("Descripció"), max_length=1000, null=True, blank=True
    )
    question = models.CharField(_("Pregunta"), max_length=1000)
    answer_0 = CharFieldWithTextarea(
        _("Resposta 1"), max_length=1000, null=True, blank=True
    )
    answer_1 = CharFieldWithTextarea(
        _("Resposta 2"), max_length=1000, null=True, blank=True
    )
    answer_2 = CharFieldWithTextarea(
        _("Resposta 3"), max_length=1000, null=True, blank=True
    )
    answer_3 = CharFieldWithTextarea(
        _("Resposta 4"), max_length=1000, null=True, blank=True
    )
    answer_4 = CharFieldWithTextarea(
        _("Resposta 5"), max_length=1000, null=True, blank=True
    )
    answer_5 = CharFieldWithTextarea(
        _("Resposta 6"), max_length=1000, null=True, blank=True
    )
    # answer poner en un model intermedio para answer Criterias/Point , parecido al roleparticipant
    icon = models.ImageField(
        upload_to="images/criteria_icons",
        verbose_name=_("Icona"),
        help_text=_("Dimensió mínima: 40 x 40px."),
        default="criteria_icons/default.png",
    )
    my_order = models.PositiveIntegerField(
        _("Ordre"), default=0, blank=False, null=False
    )

    class Meta:
        verbose_name = _("Criteri")
        verbose_name_plural = _("Criteris")
        ordering = ["my_order"]

    def __str__(self):
        return self.name


class CriteriaTranslation(models.Model):
    name = models.CharField(_("Nom traduit"), max_length=250)
    description = CharFieldWithTextarea(
        _("Descripció traduida"), max_length=1000, null=True, blank=True
    )
    language = models.CharField(
        _("Idioma traducció"),
        max_length=15,
        choices=settings.LANGUAGE_CHOICES,
        default=settings.LANGUAGE_2,
    )
    original = models.ForeignKey(
        "Criteria",
        verbose_name=_("Criteria"),
        on_delete=models.CASCADE,
        related_name="translations",
    )

    def __str__(self):
        return "{}:{}".format(self.language, self.name)

    class Meta:
        verbose_name = _("Criteria Translation")
        verbose_name_plural = _("Criteria Translations")
        ordering = ["name"]


class Comment(models.Model):
    text = models.CharField(_("Text"), max_length=3000, null=True)
    author = models.ForeignKey(
        User, verbose_name=_("Autor/a"), on_delete=models.SET_NULL, null=True
    )
    published_date = models.DateTimeField(_("Data Publicació"), auto_now_add=True)
    mappoint = models.ForeignKey(
        "MapPoint",
        verbose_name=_("Punt"),
        on_delete=models.CASCADE,
        related_name="point_comments",
    )

    class Meta:
        verbose_name = _("Comentari")
        verbose_name_plural = _("Comentaris")
        ordering = ["published_date"]

    def __str__(self):
        return "{}:{}".format(self.published_date, self.author)

    def save(self, *args, **kwargs):
        if not self.id:
            if not self.author:
                # print('Object is a new instance from form without author, ignoring since it is saved in admin overwrite')
                return

        return super(Comment, self).save(*args, **kwargs)

    # @receiver(post_save, sender='mapPointCriterias.Comment')
    # def save_comment(sender, instance, created, **kwargs):
    #         print('post saving comment')
    #         print('instance', instance)
    #         print('sender', sender)
    #         print('created', created)
    #         print('kwargs', kwargs)


#            up = UserProfile.objects.create(user=instance)
# instance.userprofile = up
# instance.save()


class LinkType(models.TextChoices):
    WEBSITE = "WEB", _("Lloc Web")
    FACEBOOK = "FACEBOOK", "Facebook"
    TWITTER = "TWITTER", "Twitter"
    INSTAGRAM = "INSTAGRAM", "Instagram"
    PINTEREST = "PINTEREST", "Pinterest"
    MASTODON = "MASTODON", "Mastodon"
    LINKEDIN = "LINKEDIN", "Linkedin"
    OTHER = "OTHER", _("Altre")


class ProfileLink(models.Model):
    profile = models.ForeignKey(
        "UserProfile",
        verbose_name=_("Perfil"),
        on_delete=models.CASCADE,
        related_name="social_links",
    )
    link_type = models.CharField(
        _("Tipus d'enllaç"),
        max_length=10,
        choices=LinkType.choices,
        default=LinkType.WEBSITE,
        null=False,
        blank=False,
    )
    url = models.URLField(_("Enllaç"), null=False, blank=False)


class PointLink(models.Model):
    point = models.ForeignKey(
        "MapPoint",
        verbose_name=_("Punt"),
        on_delete=models.CASCADE,
        related_name="point_links",
    )
    link_type = models.CharField(
        _("Tipus d'enllaç"),
        max_length=10,
        choices=LinkType.choices,
        default=LinkType.WEBSITE,
        null=False,
        blank=False,
    )
    url = models.URLField(_("Enllaç"), null=False, blank=False)


class TrinaryBoolean(models.TextChoices):
    YES = "YES", _("Si")
    NO = "NO", _("No")
    NOT_APPLY = "NOT_APPLY", _("No Aplica")


# https://realpython.com/location-based-app-with-geodjango-tutorial/#adding-geodjango
class MapPoint(models.Model):
    name = models.CharField(_("Nom"), max_length=100)
    burocracy_number = models.CharField(
        _("NIF/CIF"), max_length=20, null=True, blank=True
    )  # regexp username
    slug = models.SlugField(
        _("URL"), max_length=100, blank=False
    )  # autoguardar en Save.
    description = models.TextField(
        _("Descripció"), max_length=1200, null=True, blank=True
    )
    year_start = models.CharField(
        _("Any de Fundació"), max_length=10, null=True, blank=True
    )
    picture = models.ImageField(
        upload_to="point_images",
        verbose_name=_("Logo/Fotografia"),
        help_text=_("Dimensions mínimes recomendades: 200 x 100px."),
        default="mappoint_pictures/default.jpg",
        blank=True,
    )
    postal_code = models.CharField(
        _("Codi Postal"), max_length=10, null=True, blank=True
    )  # regexp username
    phone = models.CharField(
        _("Telèfon(s)"), max_length=40, null=True, blank=True
    )  # regexp username
    location = gis_models.PointField(
        "Coordenades per al mapa", null=True, blank=True, geography=True, srid=4326
    )
    address = models.CharField(max_length=100, null=True, blank=True)
    landlevel_1 = models.ForeignKey(
        "LandLevel_1",
        verbose_name=_("Provincia"),
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    landlevel_2 = models.ForeignKey(
        "LandLevel_2",
        verbose_name=_("Comarca"),
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    landlevel_3 = models.ForeignKey(
        "LandLevel_3",
        verbose_name=_("Municipi"),
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    # landlevel_4 = models.ForeignKey('LandLevel_4', verbose_name=_(u'Districte'))
    # landlevel_5 = models.ForeignKey('LandLevel_5', verbose_name=_(u'Barris'))
    opening_hours = models.CharField(_("Horari"), max_length=250, null=True, blank=True)
    # language = models.CharField(_(u'Idioma interfaç'), max_length=15, choices=LANGUAGE_CHOICES, default=VALENCIANO, help_text=_(u"L'idioma de l'interfaç."))
    created_date = models.DateField(_("Data Publicació"), auto_now_add=True)
    modified_date = models.DateTimeField(_("Data Modificació"), auto_now=True)
    evaluation_date = models.DateField(
        _("Data Evaluació"), null=True, blank=True
    )  # the date when the criterias evaluation was modified.
    evaluation_result_type = models.CharField(
        _("Tipus de càlcul de la puntuació"),
        max_length=20,
        choices=EvaluationResultType.choices,
        default=EvaluationResultType.SUM_ANSWERS,
    )

    status = models.CharField(
        _("Estat del formulari"),
        max_length=15,
        default=PointStatusChoices.DRAFT,
        choices=PointStatusChoices.choices,
        null=True,
    )
    editor = models.ForeignKey(
        User,
        verbose_name=_("Responsable"),
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    contact_person = models.CharField(
        _("Persona/es de contacte"), max_length=250, null=True, blank=True
    )
    email = models.EmailField(_("Correu electrònic"), null=True, blank=True)
    # xarxes socials webs
    main_category = models.ForeignKey(
        "Category",
        verbose_name=_("Sector principal"),
        related_name="points",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True,
    )
    other_categories = models.ManyToManyField(
        Category, verbose_name=_("Sectors secondaris"), blank=True
    )
    burocracy_category = models.ForeignKey(
        "BurocracyCategory",
        verbose_name=_("Sector CCAE"),
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    legal_form = models.ForeignKey(
        "LegalForm",
        verbose_name=_("Forma Jurídica"),
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    invoicing = models.CharField(
        _("Facturació anual"), max_length=250, null=True, blank=True
    )
    networks = models.ManyToManyField(
        Network, verbose_name=_("Xarxes de l'Economia Social"), blank=True
    )
    other_networks = CharFieldWithTextarea(
        _("Altres xarxes"), max_length=500, null=True, blank=True
    )
    # comments = models.ManyToManyField(Comment, related_name = 'comments', through='Comment')
    # participantRol = mostrar todas las que salgan
    # comments = models.ManyToManyField(Comment, verbose_name=_(u"Comentaris"))
    social_approval = models.CharField(
        _("Aprovat en Balanç Social?"),
        max_length=10,
        choices=TrinaryBoolean.choices,
        null=True,
        blank=True,
    )
    # fer la avaluació com un formulari especial. (q guarde puntuacions i respostes a les preguntes)

    class Meta:
        verbose_name = _("Punt")
        verbose_name_plural = _("Punts")
        ordering = ["created_date"]

    def __str__(self):
        return self.name


class MapPointTranslation(models.Model):
    name = models.CharField(_("Nom traduit"), max_length=100)
    description = CharFieldWithTextarea(
        _("Descripció traduida"), max_length=1200, null=True, blank=True
    )
    language = models.CharField(
        _("Idioma traducció"),
        max_length=15,
        choices=settings.LANGUAGE_CHOICES,
        default=settings.LANGUAGE_2,
    )
    original = models.ForeignKey(
        "MapPoint",
        verbose_name=_("MapPoint"),
        on_delete=models.CASCADE,
        related_name="translations",
    )

    def __str__(self):
        return "{}:{}".format(self.language, self.name)

    class Meta:
        verbose_name = _("Mappoint Translation")
        verbose_name_plural = _("Mappoint Translations")
        ordering = ["name"]
