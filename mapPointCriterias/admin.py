from leaflet.admin import LeafletGeoAdminMixin
from adminsortable2.admin import SortableAdminMixin


from django.contrib import admin
from django.utils.text import slugify
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User, Group

from .models import (
    MapPoint,
    MapPointTranslation,
    LandLevel_1,
    LandLevel_2,
    LandLevel_3,
    Category,
    CategoryTranslation,
    BurocracyCategory,
    LegalForm,
    Network,
    ParticipantRole,
    Criteria,
    CriteriaTranslation,
    UserProfile,
    Comment,
    PointLink,
    ProfileLink,
)

from .forms import (
    CustomCommentForm,
    CustomTranslationForm,
    save_role_participants,
    save_criterias,
)


class ProfileLinkInline(admin.TabularInline):
    # model = MapPoint.comments.through
    model = ProfileLink
    verbose_name = u"Enllaç"
    verbose_name_plural = u"Enllaços"
    extra = 0


class UserProfileInline(admin.StackedInline):
    model = UserProfile
    exclude = ["language", "community"]
    can_delete = False


class UserWithProfileAdmin(UserAdmin):
    inlines = [UserProfileInline]


admin.site.unregister(User)
admin.site.register(User, UserWithProfileAdmin)
#
# @admin.register(UserProfile)
# class UserProfileAdmin(UserAdmin):
#     list_display = ("username", "email", "is_active")
#     ordering = ("username",)
#
#     form = CustomUserCreationForm
#     filter_horizontal = ("community",)
#     inlines = [ProfileLinkInline]
#
#     fieldsets = (
#         (
#             "Autenticació",
#             {
#                 "fields": (
#                     "username",
#                     "password1",
#                     "password2",
#                     "email",
#                 ),
#                 "classes": (
#                     "order-0",
#                     "baton-tabs-init",
#                     "baton-tab-group-info--inline-social_links",
#                 ),
#                 # "description": "Un text descriptiu",
#             },
#         ),
#         (
#             "Dades personals",
#             {
#                 "fields": (
#                     "first_name",
#                     "last_name",
#                     "photograph",
#                     "consent_date",
#                 ),
#                 "classes": ("tab-info",),
#                 # "description": "Un altre text descriptiu",
#             },
#         ),
#     )
#     add_fieldsets = (
#         (
#             None,
#             {
#                 "classes": ("wide",),
#                 "fields": (
#                     "username",
#                     "email",
#                     "password1",
#                     "password2",
#                     "first_name",
#                     "last_name",
#                     "is_superuser",
#                     "is_staff",
#                     "is_active",
#                 ),
#             },
#         ),
#     )


#
# @admin.register(Language)
# class LanguageAdmin(admin.ModelAdmin):
#     list_display = ('name', 'i10n_code')

# @admin.register(Notification)
# class NotificationAdmin(admin.ModelAdmin):
#     list_display = ('title', 'show_status_change', 'show_trigger_actions')
#
#     def show_status_change(self, obj):
#         return ",".join([a.name for a in obj.status_change.all()])
#
#     def show_trigger_actions(self, obj):
#         return ",".join([a for a in obj.trigger_actions])


@admin.register(LandLevel_1)
class LandLevel1_Admin(admin.ModelAdmin):
    list_display = ("name",)


@admin.register(LandLevel_2)
class LandLevel2_Admin(admin.ModelAdmin):
    list_display = ("name", "parent")


@admin.register(LandLevel_3)
class LandLevel3_Admin(admin.ModelAdmin):
    list_display = ("name", "parent")


# @admin.register(Community)
# class CommunityAdmin(admin.ModelAdmin):
#     list_display = ("name",)


class CategoryTranslationInline(admin.TabularInline):
    # model = MapPoint.comments.through
    model = CategoryTranslation
    verbose_name = u"Traducció"
    verbose_name_plural = u"Traduccions"
    extra = 0
    form = CustomTranslationForm


class CriteriaTranslationInline(admin.TabularInline):
    # model = MapPoint.comments.through
    model = CriteriaTranslation
    verbose_name = u"Traducció"
    verbose_name_plural = u"Traduccions"
    extra = 0
    form = CustomTranslationForm


class MapPointTranslationInline(admin.TabularInline):
    # model = MapPoint.comments.through
    model = MapPointTranslation
    verbose_name = u"Traducció"
    verbose_name_plural = u"Traduccions"
    extra = 0
    form = CustomTranslationForm


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "parent",
    )
    inlines = [
        CategoryTranslationInline,
    ]
    # def formfield_for_dbfield(self, db_field, **kwargs):
    #     formfield = super(Category_Admin, self).formfield_for_dbfield(db_field, **kwargs)
    #     if db_field.name == 'description':
    #         formfield.widget = forms.Textarea(attrs=formfield.widget.attrs)
    #     return formfield


@admin.register(BurocracyCategory)
class BurocracyCategoryAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "codenumber",
    )


@admin.register(LegalForm)
class LegalFormAdmin(admin.ModelAdmin):
    list_display = ("name",)


@admin.register(Network)
class NetworkAdmin(admin.ModelAdmin):
    list_display = ("name",)


@admin.register(ParticipantRole)
class ParticipantRoleAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ("name",)


@admin.register(Criteria)
class CriteriaAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ("name",)
    exclude = ("slug",)
    readonly_fields = ("my_order",)
    # prepopulated_fields = {"slug": ("name",)}
    inlines = [
        CriteriaTranslationInline,
    ]


# @admin.register(PointStatus)
# class PointStatusAdmin(admin.ModelAdmin):
#     list_display = (
#         "name",
#         "type",
#     )


class PointLinkInline(admin.TabularInline):
    # model = MapPoint.comments.through
    model = PointLink
    verbose_name = u"Enllaç"
    verbose_name_plural = u"Enllaços"
    extra = 0
    # form = CustomCategoryTranslationForm


# @admin.register(Comment)
# class CommentAdminInline(admin.StackedInline):
class CommentInline(admin.TabularInline):
    # model = MapPoint.comments.through
    model = Comment
    verbose_name = u"Comentari"
    verbose_name_plural = u"Comentaris"
    extra = 1
    form = CustomCommentForm

    # def get_formset(self, request, obj=None, **kwargs):
    #     print("form obj")
    #
    # def get_readonly_fields(self, request, obj=None, instance_obj=None):
    #     """ comment readonly  """
    #     print("instance_obj", instance_obj)
    #     if request.user.is_superuser or instance_obj.author == request.user:
    #         return super().get_readonly_fields(request, obj)
    #     else:
    #         self.can_delete = False
    #         self.max_num = 0
    #         return list(self.readonly_fields) + ["text"]

    def has_add_permission(self, request, obj=None):
        return True

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True
        else:
            return False


def limit_list_to_owned_items(self, myclass, request, queryset, search_term):
    queryset, use_distinct = super(myclass, self).get_search_results(
        request, queryset, search_term
    )

    # do not change the name of the group as admin
    superaixadetes_group = Group.objects.get(name="SuperAixadetes")
    if (
        not request.user.is_superuser
        and superaixadetes_group not in request.user.groups.all()
    ):
        # if user is not admin or superaixadeta, can only see its own points
        queryset = queryset.filter(editor=request.user)
    return queryset, use_distinct


@admin.register(MapPoint)
class MapPointAdmin(LeafletGeoAdminMixin, admin.ModelAdmin):
    list_display = ("name", "email", "status", "editor")
    filter_horizontal = (
        "other_categories",
        "networks",
    )
    list_filter = ("status",)
    search_fields = ("name", "description")
    readonly_fields = ("created_date", "modified_date")
    prepopulated_fields = {"slug": ["name"]}
    inlines = [CommentInline, PointLinkInline, MapPointTranslationInline]
    # exclude = ("slug",)
    fieldsets = (
        (
            "Info",
            {
                "fields": (
                    "name",
                    "description",
                    "year_start",
                    "burocracy_number",
                    "picture",
                    "opening_hours",
                ),
                "classes": (
                    "order-0",
                    "baton-tabs-init",
                    "baton-tab-group-fs-contact--inline-point_links",
                    "baton-tab-fs-categories",
                    "baton-tab-fs-location",
                    "baton-tab-fs-admin",
                    "baton-tab-fs-evaluation",
                    "baton-tab-inline-point_comments",
                )
                #'description': 'Un text descriptiu'
            },
        ),
        (
            "Contacte",
            {
                "fields": ("phone", "email", "contact_person"),
                "classes": ("tab-fs-contact",),  #'tab-fs-content', ),
                # "description": "Un altre text descriptiu",
            },
        ),
        (
            "Gestió",
            {
                "fields": (
                    "main_category",
                    "other_categories",
                    "burocracy_category",
                    "legal_form",
                    "invoicing",
                    "social_approval",
                ),  #'networks', 'other_networks',
                "classes": ("tab-fs-categories",),  #'tab-fs-content', ),
                #'description': 'Un altre text descriptiu'
            },
        ),
        (
            "Territori",
            {
                "fields": (
                    "landlevel_1",
                    "landlevel_2",
                    "landlevel_3",
                    "address",
                    "location",
                    "postal_code",
                ),
                "classes": ("tab-fs-location",),  #'tab-fs-content', ),
                #'description': 'Un altre text descriptiu'
            },
        ),
        (
            "Administració",
            {
                "fields": ("created_date", "modified_date", "editor", "status", "slug"),
                "classes": ("tab-fs-admin",),
                #'description': 'Un altre altre text descriptiu'
            },
        ),
        (
            "Avaluació",
            {
                "fields": ("evaluation_date", "evaluation_result_type"),
                "classes": ("tab-fs-evaluation",),
                #'description': 'Un altre altre text descriptiu'
            },
        ),
    )
    baton_form_includes = [
        (
            "admin/evaluation.html",
            "evaluation_result_type",
            "below",
        ),
        (
            "admin/role_participants.html",
            "social_approval",
            "below",
        ),
    ]

    class Media:
        js = ("js/osm_map.js",)

    def get_search_results(self, request, queryset, search_term):
        return limit_list_to_owned_items(
            self, MapPointAdmin, request, queryset, search_term
        )

    def save_related(self, request, form, formsets, change):
        super(MapPointAdmin, self).save_related(request, form, formsets, change)
        # formset.save(commit=False) #This way, the inlines will not be saved to the database each time the main object is saved via the admin.
        # form.save_m2m()
        # obj = form.instance

        for inlines in formsets:
            for inline_form in inlines:
                if inline_form.__class__.__name__ == "CommentForm":
                    if inline_form.cleaned_data.get("id") is None:
                        text = inline_form.cleaned_data.get("text")
                        mappoint = inline_form.cleaned_data.get("mappoint")
                        if text and mappoint:
                            # saving new comment

                            c = Comment.objects.create(
                                text=text,
                                mappoint=mappoint,  # form.instance,
                                author=request.user,
                            )
                            inline_form.cleaned_data["id"] = c

    def save_model(self, request, obj, form, change):
        if not change:
            obj.editor = request.user
            obj.slug = slugify(obj.name)

        super(MapPointAdmin, self).save_model(request, obj, form, change)
        save_role_participants(request, obj)
        save_criterias(request, obj)

    def has_add_permission(self, request, obj=None):
        superaixadetes_group = Group.objects.get(name="SuperAixadetes")
        if (
            request.user.is_superuser
            or superaixadetes_group in request.user.groups.all()
        ):
            return True
        else:
            return False


# This has to be down here
from baton.autodiscover.admin import BatonAdminSite

BatonAdminSite.site_url = "/admin/"
# BatonAdminSite.index_template = 'baton/index.html'


site = BatonAdminSite()  # noqa
