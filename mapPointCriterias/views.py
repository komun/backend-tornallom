from collections import defaultdict
import json

from django.core.paginator import Paginator
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q
from django.conf import settings
from django.db.models import CharField, Value

from .models import (
    Category,
    MapPoint,
    CategoryTranslation,
    EvaluationCriteriasForMapPoint,
    PointLink,
    Criteria,
    CriteriaTranslation,
    PointStatusChoices,
    LandLevel_3,
)


def getSectors(request, language):
    translated_names = {}
    qs = Category.objects.filter(parent_id__isnull=True)
    original_items = list(qs.values())
    if language not in [settings.DEFAULT_LANGUAGE, "ca"]:

        ids = Category.objects.values_list("id", flat=True)
        qs = CategoryTranslation.objects.filter(original__id__in=ids, language=language)

        for i in qs:
            translated_names[i.original_id] = {
                "name": i.name,
                "description": i.description,
            }

    for i in original_items:
        original_id = i["id"]
        if original_id in translated_names:
            i.update(translated_names[original_id])

    return JsonResponse(original_items, safe=False)


def getTerritories(request, language):
    # add the "type:TOWN" parameter
    qs = (
        LandLevel_3.objects.all()
        .values("id", "name")
        .annotate(type=Value("TOWN", output_field=CharField()))
    )
    response_json = {"response": list(qs), "status": 200}
    return JsonResponse(response_json, safe=False)


def getCriterias(request, language):
    translated_names = {}
    json_items = []
    qs = Criteria.objects
    if language and language not in [settings.DEFAULT_LANGUAGE, "ca"]:
        ids = qs.values_list("id", flat=True)
        qs_trans = CriteriaTranslation.objects.filter(
            original__id__in=ids, language=language
        )

        for i in qs_trans:
            translated_names[i.original_id] = {
                "name": i.name,
                "description": i.description,
            }

    for cri in qs.all():
        item_json = {
            "id": cri.id,
            "name": cri.name,
            "description": cri.description,
            "iconUrl": cri.icon.url,
        }
        if cri.id in translated_names:
            item_json.update(translated_names[cri.id])
        json_items.append(item_json)

    response_json = {"response": json_items, "status": 200}
    return JsonResponse(response_json, safe=False)


def getComplyingCriterias(points):
    evals = EvaluationCriteriasForMapPoint.objects.filter(
        mappoint__in=points, dont_apply=False
    ).select_related("mappoint", "criteria")
    evals_ok = [eval for eval in evals if eval.is_ok]
    complyingcriterias = defaultdict(list)
    for eval in evals_ok:
        complyingcriterias[eval.mappoint.id].append(eval.criteria.icon.url)
    return complyingcriterias


def getLinks(points):
    links = defaultdict(dict)
    links_qs = PointLink.objects.filter(point__in=points)
    for link in links_qs:
        links[link.point.id][link.link_type.lower()] = link.url
    return links


def getPointJson(point, language):
    json = {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [
                float(point.location.coords[0]),
                float(point.location.coords[1]),
            ],
        },
        "properties": {
            "address": point.address,
            "town": getattr(point.landlevel_3, "name", ""),
            "sector": point.main_category.name
            if hasattr(point.main_category, "name")
            else "",
            "sectorMapIconUrl": point.main_category.mapicon.url
            if hasattr(point.main_category, "mapicon")
            else "",
            "sectorIconUrls": [point.main_category.icon.url]
            if hasattr(point.main_category, "icon")
            else [""],
            "pictureUrl": point.picture.url,
            "description": point.description,
            "phone": point.phone,
            "name": point.name,
            "openingHours": point.opening_hours,
            "id": point.id,
            "normalizedName": point.slug,
            "email": point.email,
            "status": point.status,
        },
    }

    if language not in [settings.DEFAULT_LANGUAGE, "ca"]:
        tra = point.translations.filter(language=language).first()
        if tra:
            json["properties"]["name"] = tra.name
            json["properties"]["description"] = tra.description

        if point.main_category:
            tra = point.main_category.translations.filter(language=language).first()
            if tra:
                json["properties"]["sector"] = tra.name

    return json


def getSectorInfo(s, language):
    # remove this line to use the subsectors' icons too.
    parent = s.parent if s.parent else s
    json = {
        "id": s.id,
        "name": s.name,
        "description": s.description,
        "iconUrl": parent.icon.url,
        "mapIconUrl": parent.mapicon.url,
    }
    if language not in [settings.DEFAULT_LANGUAGE, "ca"]:
        tra = s.translations.filter(language=language).first()
        if tra:
            json["name"] = tra.name
            json["description"] = tra.description
    return json


def getPointSectors(point, language):
    sectors = []
    main_category_id = getattr(point.main_category, "id", 0)

    all_sectors = point.other_categories.all()
    for s in all_sectors:
        if s.id != main_category_id:
            sector_json = getSectorInfo(s, language)
            sectors.append(sector_json)
    return sectors


def getPointDirectorySearch(point, language):
    json = {
        "id": point.id,
        "nif": point.burocracy_number,
        "name": point.name,
        "normalizedName": point.slug,
        "description": point.description,
        "pictureUrl": point.picture.url,
        "address": point.address,
        "registryDate": 0,
        "entityStatus": {"name": point.status},
        "oldVersion": False,
    }
    if point.landlevel_1:
        json["province"] = {"id": point.landlevel_1.id, "name": point.landlevel_1.name}

    if point.main_category:
        json["mainSector"] = {
            "id": point.main_category.id,
            "name": point.main_category.name,
            "description": point.main_category.description,
            "iconUrl": point.main_category.icon.url,
            "mapIconUrl": point.main_category.mapicon.url,
        }
    if language not in [settings.DEFAULT_LANGUAGE, "ca"]:
        tra = point.translations.filter(language=language).first()
        if tra:
            json["name"] = tra.name
            json["description"] = tra.description

        if point.main_category:
            tra = point.main_category.translations.filter(language=language).first()
            if tra:
                json["mainSector"]["name"] = tra.name
                json["mainSector"]["description"] = tra.description
    return json


def getProfileDetails(point):

    json = {
        "longitude": float(point.location.coords[0]),
        "latitude": float(point.location.coords[1]),
        "phone": point.phone,
        "openingHours": point.opening_hours,
        "email": point.email,
    }
    if point.landlevel_2:
        json["region"] = {"id": point.landlevel_2.id, "name": point.landlevel_2.name}
    if point.landlevel_3:
        json["town"] = {"id": point.landlevel_3.id, "name": point.landlevel_3.name}

    return json


def searchEntities(request, language):
    json_points = []

    if request.body:
        print("body", request.body)
        filters = json.loads(request.body)
        f_page = int(filters.get("page", 0)) + 1
        f_size = int(filters.get("size", 12))
        f_territoryId = filters.get("territoryId", None)
        f_territoryType = filters.get("territoryType", "TOWN")
        f_sectorIds = filters.get("sectorIds", [])

        # territoryId: 127
        # territoryType: TOWN
        # sectorsIds:[33] //alimentació
        response_json = {"response": []}
        points = (
            MapPoint.objects.filter(
                location__isnull=False, status=PointStatusChoices.PUBLISHED
            )
            .order_by("slug")
            .select_related("main_category", "landlevel_3")
        )
        if f_sectorIds:
            points = points.filter(
                Q(main_category__in=filters["sectorIds"])
                | Q(main_category__parent__in=filters["sectorIds"])
            )
        if f_territoryId:
            if f_territoryType == "TOWN":
                points = points.filter(landlevel_3=f_territoryId)
            elif f_territoryType == "REGION":
                points = points.filter(landlevel_2=f_territoryId)
            elif f_territoryType == "PROVINCE":
                points = points.filter(landlevel_1=f_territoryId)

        if "text" in filters and filters["text"]:

            points = points.filter(
                Q(name__icontains=filters["text"])
                | Q(description__icontains=filters["text"])
            )

        pages = Paginator(points, f_size)
        content = []
        if f_page > pages.num_pages:
            page_points = []
        else:
            page_points = pages.page(f_page).object_list
            for point in page_points:
                json_point = getPointDirectorySearch(point, language)

                json_point["sectors"] = getPointSectors(point, language)
                content.append(json_point)

        json_points = {
            "totalElements": len(page_points),
            "lastPage": pages.num_pages <= f_page,
            "content": content,
        }

    response_json = {"response": json_points, "status": 200}
    return JsonResponse(response_json, safe=False)


def getEvaluationResults(point):

    results = []
    for eval in point.evaluations.all().select_related("criteria"):

        json = {
            "criterion": {
                "id": eval.criteria_id,
                # not needed, taken by the other API /criteria call
                # "name": eval.criteria.name,
                # "iconUrl": eval.criteria.icon.url,
            },
        }

        if eval.dont_apply:
            overal_points = -1
        else:
            if eval.total_points is None:
                overal_points = -1
            else:
                overal_points = int(eval.total_points * 100)

        if overal_points != 0:
            json["level"] = overal_points

        results.append(json)

    return {"id": 5849, "date": 1571788800000, "entityEvaluationCriterions": results}


def getEntity(request, language, entity):
    qs = MapPoint.objects.filter(slug=entity).select_related(
        "landlevel_1", "landlevel_2", "landlevel_3", "main_category"
    )

    if not qs:
        response_json = {"response": [], "status": 404}
    else:
        point = qs.first()
        json_point = getPointDirectorySearch(point, language)
        json_point.update(getProfileDetails(point))
        json_point["sectors"] = getPointSectors(point, language)
        for link in point.point_links.all():
            json_point[link.link_type.lower()] = link.url

        json_point["entityEvaluation"] = getEvaluationResults(point)

        response_json = {"response": json_point, "status": 200}

    return response_json


def relatedEntitiesBySector(request, language, param):
    response_json = {"response": param, "status": 200}
    return response_json


def relatedEntitiesByProximity(request, language, param):
    response_json = {"response": param, "status": 200}
    return response_json


def searchEntitiesGeojson(request, language):
    json_points = []

    if request.body:
        print("body", request.body)
        filters = json.loads(request.body)
        points = MapPoint.objects.filter(location__isnull=False)
        if "entityStatusTypes" in filters and filters["entityStatusTypes"]:
            points = points.filter(
                status__in=filters["entityStatusTypes"], location__isnull=False
            )

        if "sectorIds" in filters and filters["sectorIds"]:
            points = points.filter(
                Q(main_category__in=filters["sectorIds"])
                | Q(main_category__parent__in=filters["sectorIds"])
            )
        if "text" in filters and filters["text"]:

            points = points.filter(
                Q(name__icontains=filters["text"])
                | Q(description__icontains=filters["text"])
            )
        points = points.select_related("main_category", "landlevel_3")

        complyingcriterias = getComplyingCriterias(points)
        links = getLinks(points)
        for p in points:
            json_point = getPointJson(p, language)
            json_point["properties"]["criteriaIconUrls"] = complyingcriterias[p.id]
            json_point["properties"].update(links[p.id])

            json_points.append(json_point)

    response_json = {"response": json_points, "status": 200}
    return JsonResponse(response_json, safe=False)


def entitiesGeojson(request, language, param):
    return {
        # "response": {
        #     "type": "Feature",
        #     "geometry": {
        #         "type": "Point",
        #         "coordinates": [0.6386742667018553, 41.81400237329747],
        #     },
        #     "properties": {
        #         "address": "",
        #         "town": "",
        #         "pictureUrl": "/image/7313",
        #         "facebook": "",
        #         "sectorMapIconUrl": "/image/8972",
        #         "description": "",
        #         "pinterest": "",
        #         "instagram": "",
        #         "twitter": "",
        #         "criteriaIconUrls": [
        #             "/image/5270",
        #             "/image/5271",
        #         ],
        #         "sectorIconUrls": ["/image/8971"],
        #         "phone": "",
        #         "web": "",
        #         "quitter": "",
        #         "name": "",
        #         "openingHours": "De dilluns a dissabte de 9:30 a 14:00 h",
        #         "id": 6530,
        #         "normalizedName": "",
        #         "sector": "Alimentació - Ramaderia i pesca",
        #         "email": "",
        #         "status": "PUBLISHED",
        #     },
        # },
        # "status": 200,
    }


@csrf_exempt
def api_endpoint_with_param(request, endpoint, param):
    print("API Endpoint called: ", endpoint)
    response = JsonResponse(data={"error": "API Method not found"}, status=404)
    print("GET", request.GET)
    language = request.GET.get("lang", "")
    if endpoint == "entities":
        response = getEntity(request, language, param)
    elif endpoint == "entitiesGeojson":
        response = entitiesGeojson(request, language, param)
    elif endpoint == "relatedEntitiesBySector":
        response = relatedEntitiesBySector(request, language, param)
    elif endpoint == "relatedEntitiesByProximity":
        response = relatedEntitiesByProximity(request, language, param)

    return JsonResponse(response, safe=False)


# /api/relatedEntitiesBySector//3
# /api/relatedEntitiesByProximity//3


@csrf_exempt
def api_endpoints(request, endpoint):
    print("API Endpoint called: ", endpoint)
    language = request.GET.get("lang", "")

    response = JsonResponse(data={"error": "API Method not found"}, status=404)
    print("GET", request.GET)
    print("POST", request.POST)
    if endpoint == "mainSectors":
        response = getSectors(request, language)
    elif endpoint == "territories":
        response = getTerritories(request, language)

    elif endpoint == "searchEntitiesGeojson":
        response = searchEntitiesGeojson(request, language)
    elif endpoint == "searchEntities":
        response = searchEntities(request, language)
    elif endpoint == "criteria":
        response = getCriterias(request, language)

    return response
