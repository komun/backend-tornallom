EVALUATIONS = {
    "detroit-llibres-alcoi-alcoy": {
        "editor": "Tornallom",
        "postal_code": "03802",
        "what_to_find": "presentacions de llibres, llibres, llibreria",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "2,017",
        "comments": [
            {
                "date": "2019-07-21 11:33:44",
                "commenter": "Tornallom",
                "text": """Proyecto: Detroit Llibres (Tòfol i Felip)
Lugar: Alcoi
Fecha de la visita: 19 julio 2019
Duración: 1h 15 min
Aixadetes: Tereseta, Patricia y Pedro
Persona de enlace: Tereseta

Descripción breve de la visita: Llegamos a la librería a las 20h, cuando están a punto de cerrar. Nos sentamos en una mesa al fondo y les contamos el proyecto, del que ya han oído hablar. Entran algunas personas a la tienda y por momentos alguno de ellos se levanta, pero no supone interrupciones importantes.

Valoración de la entrevista. Anotaciones y reflexiones:
Hubiera estado bien tener al menos media hora más, porque las últimas preguntas tenemos que hacerlas muy rápido, pero ha dado tiempo a explicar ciertas cosas bien (balance social, anécdotas, ejemplos de buenas prácticas…).


Nuevos contactos de proyectos a mapear:
- Xarxa Agroecològica d’Alcoi
- La Finca Ecològica (Jordi i Lourdes)
""",
            }
        ],
        "ratios": {
            "Persones remunerades": "0,3,0",
            "Càrrecs directius": "0,2,0",
            "Òrgans de govern": "",
            "Persones no remunerades": "",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """decisions per consens.

de moment són dos persones però al setembre s'incorporarà una tercera persona al projecte en condicions d'igualtat. exporàdicament, contracten a persones per donar un cop de ma.""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """malgrat que volíen una dona per equilibrar el projecte, finalment no ha estat possible la seua contractació.
en la llibreia visibilitzen apartartat feminista i tenen llibres infantils que trenquen estereotips (són proactius en continguts de gènere)
organitzen el local activitats amb altres col·lectius feministes""",
            },
            "condicions-laborals": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """estan començant i son conscients de la seua "autoexplotació".""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """tenen una cooperació entre llibreries d'Alcoi i a més a més, formen part de la xarxa de llibrerries crítiques.""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "transparencia": {
                "no-aplica": "X",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """tenen inquietuts al respecte pendents de solventar.""",
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "x",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "x",
                "p2": "x",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "x",
                "p5": "",
                "p6": "x",
                "notas": "son socis de https://www.seneo.org/ (Ontinyent)",
            },
        },
    },
    "fruitec": {
        "editor": "Tornallom",
        "postal_code": "03409",
        "what_to_find": " alimentació, agricultura ecològica, hortalisses, cereals, llegums, cistelles, verdures, fruita, fruites, ecològic agroecològic, fruita, formació, cooperatives de consum, cistelles, proximitat, venda directa, hort, ous, gallines, a granel",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "",
        "comments": [
            {
                "date": "2019-07-21 11:20:02",
                "commenter": "Tornallom",
                "text": """hem tingut un bon debat respecte si visibilitzar  o no aquest mapa i finalment hem consensuat fer-ho per tal de donar-li una possibilitat de millora donat el potencial de l'iniciativa.
pensem que aquest formulari està molt pensat des de la ciutat, molt "urbanita"... i que es queda un poc curt a l'hora d'avaluar espais rurals.""",
            },
            {
                "date": "2019-07-21 11:32:40",
                "commenter": "Tornallom",
                "text": """Proyecto: FRUITEC (Pepe Ferrándiz)
Lugar: La Canyada
Fecha de la visita: 20 de julio 2019
Duración: 4h
Aixadetes: Tereseta, Patricia y Pedro
Persona de enlace: Tereseta

Descripción breve de la visita: Nos encontramos en el almacén y nos vamos a almorzar a Biar. En el almuerzo se suma Vicent, un agricultor jubilado que hace aceite en la cooperativa de Biar. Después visitamos algunas fincas de manzanas, peras, etc. y el almacén y, mientras, Pepe nos va explicando cómo funciona la empresa. Terminamos en la oficina, revisando el formulario y haciéndole algunas preguntas, pero esta parte de la visita no fue en las mejores condiciones, porque ya era tarde y el estaba con su nieto.

Valoración de la entrevista. Anotaciones y reflexiones:



Nuevos contactos de proyectos a mapear:
- Cooperativa de Biar (contacto Vicent Martínez 616282215)
- Eloisa y Kiko (Dénia) 646227681. Hortalizas, puesto en el mercado central de Dénia.""",
            },
        ],
        "ratios": {
            "Persones remunerades": "1,3",
            "Càrrecs directius": "0,1",
            "Òrgans de govern": "0,1",
            "Persones no remunerades": "",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "condicions-laborals": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "x",
                "p2": "",
                "p3": "",
                "p4": "x",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "X",
                "p4": "",
                "p5": "x",
                "p6": "",
                "notas": """""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "x",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
        },
    },
    "zafra-monover-monovar": {
        "editor": "Tornallom",
        "postal_code": "03640",
        "what_to_find": "",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "",
        "comments": [{"date": "", "commenter": "", "text": """"""}],
        "ratios": {
            "Persones remunerades": "1,1",
            "Càrrecs directius": "",
            "Òrgans de govern": "",
            "Persones no remunerades": "",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "",
                "p1": "x",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "X",
                "p6": "",
                "notas": """Tereseta, 4 i 5 (2,5 final)
Pedro, 3,5-4 creo que el 1 tambien lo cumplen de alguna manera,
Patri  4  (para ser dos personas hacen las cosas lo mejor possible)
Raquel  2-2,5 (habian reflexionado sobre el tema, lo tienen en cuenta)""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "X",
                "p5": "X",
                "p6": "",
                "notas": """Tereseta (2,5 - 3 final) d’acord amb Raquel; encara és un tema que han de millorar. Mar va dir alguna cosa en pla: «s’enfadem, estem uns dies sense parlar i au...» Potser siga un tema que s’han de treballar un poquet més
Pedro, 3-3,5 por ser solo 2 y pareja se facilita la conciliación de forma autómatica por lo que el 2 también lo cumplen en parte
Patri    idem Pedro
Raquel 2, 2.5 (creo que si participara más gente lo cumplirían todo pero al ser 2 y pareja... les podría algo basico)""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "x",
                "p6": "",
                "notas": """-Raquel 2,  (  han reflexionado y lo tiene en cuenta además Mar es la car visible y comparten responsasilidades por igual ,creo que si participara más gente lo cumplirían todo pero al ser 2 y pareja... les podría algo basico)
-pedro 3,5-4 la 3 y la 5 la cumplen claramente, no precisasn protocolos por ser 2 y pareja ambos personas maduras que comprenden y defienden los principios feministas
- Patricia: 3,5   para mi cumplen el punto 2, 3 y 4 y el 5 a medias.
-Tereseta (3,5 final) d’acord amb Patri.
""",
            },
            "condicions-laborals": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """-Tereseta (2 final) estan començant, projecte que comença encara no és 100% productiu... idem que Raquelilla.
Patri  2,5
Pedro 2,5-3 la 1 la 2 y la 3 la cumplen y e resto por que se encuentran en fase de construccion del proyecto y asentamiento
Raquel 1 Han reflexioando sobre el tema pero son solo 2""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "x",
                "p6": "",
                "notas": """Raquel: 4 (recordar lo del corcho de las botellas)
pedro 4- 4,5
Patricia: 4,5, (para mi podrían ser perfectamente un referente en esto)
Tereseta (4 final)""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "x",
                "p4": "X",
                "p5": "X",
                "p6": "",
                "notas": """Raquel: 2
pedro 3 – 3,5 el 3  y el 4 creo que lo cumplen  en parte al participar en spg , mercadillos..
Patricia: 3,5. Están también en asociaciones de vinos naturales, etc.
Tereseta (3 final) sí, entre un 3 i un 3,5...""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "X",
                "p5": "",
                "p6": "x",
                "notas": """Raquel: 2.5 no recuerdo ben però
pedro 2,5 tampoco me acuerdo
Patricia:  Usan linux. Lo que diga Tereseta.
Tereseta (entre 2 i 2,5 final) és que hi ha poc marge; però totsdos crec que estan prou sensibilitzats, llavors potser millor un 2,5""",
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "x",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "x",
                "p6": "",
                "notas": """Raquel no me acuerdo nada sobre esto :S
pedro 3- 3,5  la 1, 2 ,3  la cumplen y creo que han hecho el balance básico, spg,, tornallom
Patricia: 3,5
Tereseta (3 final) van dir que enguany se’ls va oblidar fer el balanç social. Tenen poca estructura i encara els salaris depenen de subvencions i ajuda externa... no sé. Màxim un 3, de la meua part.""",
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """Raquel 0.5-1  Porque al menos han buscado que sea una entidad que esté en el pueblo y so nsea Sabadell, aunque no operen con tiodos podrían tener una cuenta abierta donde sea para ingresar.
Pedro: idem que raquel
Patricia: yo no me acuerdo de este punto. Les volvería a preguntar.
Tereseta (! tampoc me’n recorde txe... però no era una puntuació alta).""",
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """Puntuación:
Raquel 0
pedro 0
Patricia: yo les pondría al menos 0,5 puntos porque estoy segura de que lo han reflexionado.
Tereseta (?) el tema del braile en les etiquetes el van comentar, però els troquels son cars... i de moment imprieixen en el poble i son etiquetes manuals, poca quantitat. No sé, 0,5 per exemple.""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "X",
                "p5": "X",
                "p6": "",
                "notas": """Raquel 2-3 (no recuerdo si la 1 lo cumplen o no)
pedro  4,  creo que el proyecto en si mismo en un ejemplo de contribución a la transformación social
Patricia, 4 sin duda
Tereseta (4 també)""",
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "X",
                "p5": "x",
                "p6": "",
                "notas": """Raquel 3.5 (creo que si el punto 5. No lo hacen es por falta de tiempo al ser un proyecto nuevo)
pedro idem que Raquel,
Patricia: Yo les pondría mínimo 4 por el valor de recuperar la actividad del vino en esa zona, más aún en secano. Conocen a todos los actores del territorio, restaurantes, etc... Diseño de etiquetas lo ha hecho alguien del pueblo... para mi serán referencia en este punto en unos años.
Tereseta (també, de 3,5 a 4)""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "x",
                "p5": "X",
                "p6": "x",
                "notas": """Raquel 3.5
pedro 4-4,5  creo que el proyecto en si mismo en un ejemplo de contribución a la sostenibilidad
Patricia: 4,5
Tereseta (un 4, per ficar una coseta intermitja i no ficar tant de mig... hehehe).""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """Raquel: 2 (no recuerdo bien esto)
pedro idem
Patricia: 2,5 o 3. Han aprovechado maquinaria y recursos de la família que han reparado. Vidrio local, cajas y bolsas de cartón, corcho en vez de plástico, etc.
Tereseta (igual que Patri)""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Raquel 1 ¿estaban con Som enegia?
Pedro 2-2,5  creo que la 1 yla 3 la cumplen y el resto no aplica mucho
Patricia: idem Pedro.
Tereseta (un 2 màxim).""",
            },
        },
    },
    "camperola-elx-elx-elche": {
        "editor": "Tornallom",
        "postal_code": "03201",
        "what_to_find": "",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "",
        "comments": [{"date": "", "commenter": "", "text": """"""}],
        "ratios": {
            "Persones remunerades": "6,9",
            "Càrrecs directius": "",
            "Òrgans de govern": "",
            "Persones no remunerades": "5,1",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """""",
            },
            "condicions-laborals": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "x",
                "p5": "x",
                "p6": "",
                "notas": """""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "x",
                "p4": "X",
                "p5": "x",
                "p6": "",
                "notas": """""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "x",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "X",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "x",
                "p5": "",
                "p6": "x",
                "notas": """""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "x",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "x",
                "p2": "x",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
        },
    },
    "camperola-santa-pola": {
        "editor": "Tornallom",
        "postal_code": "03130",
        "what_to_find": "",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "",
        "comments": [{"date": "", "commenter": "", "text": """"""}],
        "ratios": {
            "Persones remunerades": "6,9",
            "Càrrecs directius": "",
            "Òrgans de govern": "",
            "Persones no remunerades": "5,1",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """""",
            },
            "condicions-laborals": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "x",
                "p5": "x",
                "p6": "",
                "notas": """""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "x",
                "p4": "X",
                "p5": "x",
                "p6": "",
                "notas": """""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "x",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "X",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "x",
                "p5": "",
                "p6": "x",
                "notas": """""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "x",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "x",
                "p2": "x",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
        },
    },
    "mercatremol-alacant-alicante": {
        "editor": "",
        "postal_code": "03010",
        "what_to_find": "",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "2,008",
        "comments": [{"date": "", "commenter": "", "text": """"""}],
        "ratios": {
            "Persones remunerades": "1,1",
            "Càrrecs directius": "",
            "Òrgans de govern": "",
            "Persones no remunerades": "",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "X",
                "p4": "x",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "x",
                "p2": "x",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """
Hay consenso en que es un área de grandes carencias a pesar de reconocerse un
«espíritu» sensible y favorable a la lucha contra la discriminación y el abuso machista
inherente a la cultura de nuestra sociedad y del cual no estamos exentas aunque no
seamos conscientes. Se hacen cosas simbólicas como el apoyo a las manifestaciones
del 8 de octubre con decision de la junta directiva de cerrar ese día manteniendo la
retribución de lxs trabajadorxs""",
            },
            "condicions-laborals": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "X",
                "p5": "",
                "p6": "x",
                "notas": """""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "x",
                "p5": "x",
                "p6": "x",
                "notas": """Claramente no existe procesos de participación de los proveedores en
la política de precios, eleccion de productos, si bien es cierto que no se discuten los
precios y existe una relación de confianza que va más allá de la económica y mercantil.
Puntualmente se divulgan y
apoyan iniciativas y luchas por la justicia social y en concreto se apoya al movimiento
por la soberanía alimentaria vendiendo su revista entre lxs socias""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "x",
                "p2": "X",
                "p3": "",
                "p4": "X",
                "p5": "",
                "p6": "x",
                "notas": """""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "x",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "x",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "x",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """""",
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "",
                "p2": "x",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """Se participa en la asociación de vecinos, fiestas del barrio (no mucho, solo
con apoyo en el llibret, y poco más..) se comparte el espacio de la asociacion de
vecinos del barrio para eventos""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "x",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """""",
            },
        },
    },
    "riera-agres-granja-escola-agres": {
        "editor": "Tornallom",
        "postal_code": "03837",
        "what_to_find": "",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "2,000",
        "comments": [{"date": "", "commenter": "", "text": """"""}],
        "ratios": {
            "Persones remunerades": "5,5",
            "Càrrecs directius": "1,1",
            "Òrgans de govern": "",
            "Persones no remunerades": "",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """Después de cada jornada, se reunen para comentar cómo ha ido""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "x",
                "p4": "x",
                "p5": "",
                "p6": "",
                "notas": """Aunque no tiene un mecanismo de resolución de conflictos, de conciliación familiar e gestión del equipo lo hacen. Ejemplos:

-Tuvieron un trabajador que tuvo un hijo y se llevaba al bebe al trabajo
-Después de cada día, se reunen para ver cómo ha ido la jornada y si hay mal entendidos o estrés hablarlo.
-Consultan a cada trabajador/a qué pueden aportar o intereses que tienen en trabajar""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Han tenido debates internos y son conscientes que hay roles difíciles de romper... cómo que todas las limpiadoras son mujeres, ect. pero contratan independientemete sexo o condición,""",
            },
            "condicions-laborals": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "",
                "p4": "X",
                "p5": "",
                "p6": "x",
                "notas": """""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "x",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "",
                "p4": "X",
                "p5": "",
                "p6": "x",
                "notas": """""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "x",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """""",
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "x",
                "p6": "",
                "notas": """""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "x",
                "p2": "x",
                "p3": "X",
                "p4": "X",
                "p5": "x",
                "p6": "x",
                "notas": """Han buscado la manera de poner placas pero no les ha sido posible, Tienen unpozo dónde recogen el agua de lluvia, utilizan LEDS, todo los alimentos se reutilizan para los animales o compost, Solo utilizan platos y vasos que no sean de plástico, compran yogures grandes para reducir cantidad de plástico""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "x",
                "p2": "x",
                "p3": "X",
                "p4": "x",
                "p5": "X",
                "p6": "x",
                "notas": """""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "x",
                "p4": "x",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
        },
    },
    "biotremol": {
        "editor": "Tornallom",
        "postal_code": "03690",
        "what_to_find": "",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "2,012",
        "comments": [{"date": "", "commenter": "", "text": """"""}],
        "ratios": {
            "Persones remunerades": "",
            "Càrrecs directius": "",
            "Òrgans de govern": "",
            "Persones no remunerades": "",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """Hay 2 tipos de socios: 1)Activista con derecho a voto. Se decide por consenso y 2)Consumidores pueden asistir pero no tienen derecho a voto""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "x",
                "p5": "",
                "p6": "",
                "notas": """Pagaron un curso de informática para una trabajadora para formarse en la cooperativa.

Una vez al mes quiren hacer reuniones donde se trate las emociones y los estados y evaluar cargas de trabajo.""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Consideran que n  o hacae falta la perspectiva feminista porque se da de manera natural al ser mayoría mujeres (;S)""",
            },
            "condicions-laborals": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "X",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "X",
                "p4": "x",
                "p5": "x",
                "p6": "",
                "notas": """Hacen visitas a los productores """,
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "x",
                "p5": "",
                "p6": "",
                "notas": """Están en el grupo del Mercado Social pero no están activos.
No tienen caja de resistencia pero la cuota de socio la puedes cambiar por horas de trabajo""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "x",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Están el proceso de utilizar programas de liciencias libres. Asistiron al taller de Soberania Tecnològica de Tornallom""",
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "X",
                "p5": "x",
                "p6": "",
                "notas": """Han hecho el Balance Social pero no lo han publicado, aunque lo quieren hacer""",
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "x",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Intentan no pedir crédito, reunen el dinero con el apoyo de los socias/os. Tienen Triodos pero operan con BBVA/ Seguros Atlantis""",
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Quieren ver qué pueden hacer con la comida que sobra/desecha. Quieren hacer un taller con mujeres de exclusión social""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "x",
                "p2": "X",
                "p3": "",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """Aunque la coop no es Anticapitalista, colaboran con movimientos como TTIP, Friday for Future y Reiniciando. Están dentro de REAS-PV""",
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "",
                "p2": "x",
                "p3": "x",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """No fomentan el valenciano, ni escriben la web en valenciano, ni los boletines. No hacen nada vecinal, ni por la defensa del territorio.
Si alguien les llama para dar una charla van, pero no son proActivos.""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "x",
                "p5": "",
                "p6": "",
                "notas": """Hacen algo de incidencia política con el boletín. Han puerto surtidores para reducir los envases""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "x",
                "p6": "",
                "notas": """""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
        },
    },
    "altur": {
        "editor": "Tornallom",
        "postal_code": "03690",
        "what_to_find": "",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "2,014",
        "comments": [{"date": "", "commenter": "", "text": """"""}],
        "ratios": {
            "Persones remunerades": "",
            "Càrrecs directius": "",
            "Òrgans de govern": "",
            "Persones no remunerades": "",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "x",
                "notas": """Consejo rector, comisiones de trabajo semanales y mensual voluntaria.Orden del dia, acta, lectura del acta anterior.
""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "x",
                "p5": "",
                "p6": "x",
                "notas": """permisos remunerados justificados.
"la cooperativa me cambio la vida", formación, etc. "somos todos iguales".
Reducción progresiva de horas. Horas extras que se anotan en una bolsa pero ya no más, solo para mejoras, activismo. Si en 4 años alcanzas x número se te reduce la jornada laboral. Evaluación de competencias obligada para ver qué carencias tiene cada uno. Hay a partir de esto pequeñas diferencias de sueldos.
Hay un mecanismo de resolución de conflictos.""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """Sector particular. Roles muy marcados. Reflexión pendiente.
Becas a mujeres para que haya más.""",
            },
            "condicions-laborals": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """hay poco margen en el sector de la construcción. Seguros sí, telefonía.
Material de oficina.
Proximidad de proveedores, aunque eso no hace mucho cambio.""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "X",
                "p6": "",
                "notas": """Barrio del cementerio. Jornada anual de rescate, pioneros. Colaboración entre empresas, intercambio de herramientas...
Han asesorado a cooperativas.
Estatal federación de verticaleros y aqui estan tratando de hacer una, iniciativa de ellos.
proyecto del cementerio.""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "x",
                "p2": "x",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """EStán empezando, pero les cuesta. A nivel personal sí. Alfonso.""",
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "X",
                "p6": "x",
                "notas": """Acceso a todas las actas, google drive
No en la web.""",
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """Cooperativa de crédito caja rural de orihuela y FIARE para ahorros.
""",
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "X",
                "p6": "X",
                "notas": """Becas para personas en riesgo de inclusión social y refugiados. Contacto con asociaciones de refugiados. Lenguaje no es barrera.
Sancionan comportamientos discriminatorios,
Limpieza a una empresa de diversidad funcional.""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "X",
                "p6": "",
                "notas": """Sin ánimo de lucro. Limitación salarial. REAS.
Ticket de mercatrèmol.""",
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "x",
                "p2": "x",
                "p3": "x",
                "p4": "X",
                "p5": "X",
                "p6": "",
                "notas": """se tiene en cuenta el valenciano en la evaluación de competencias, se apoya que quien lo habla lo haga de forma habitual. Proyecto de mejora para traducir.
Asociación de empresas del polígono estuvieron pero no les gustó la actitud, anticooperación. """,
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """Osmosis, reciclan el agua, no compran botellas, papel reciclado.
Fomentan el consumo responsable, como fairphone.
Huelga ambiental.""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """REuniones para que pongan contenedores en el polítgono y para no tirar residuos peligrosos en el contenedor.
Protocolo interno, formación de gestión de residuos. Se encuentran muro total.
Vehiculos compartidos""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "X",
                "p6": "x",
                "notas": """""",
            },
        },
    },
    "xarxa-agroecologica-alcoi-alcoi-alcoy": {
        "editor": "Tornallom",
        "postal_code": "",
        "what_to_find": "",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "2,009",
        "comments": [
            {
                "date": "2019-10-24 18:53:01",
                "commenter": "Tornallom",
                "text": """fan 2 o 3 assemblees a l'any; màxim acudeixen unes 15 - 20 persones, les més actives. volen millorar aquest aspecte. Tenen 80 socis. L'activitat s'ha replicat en els últims 3 anys, coincidint amb el canvi de local.""",
            },
            {
                "date": "2019-10-28 15:29:08",
                "commenter": "Tornallom",
                "text": """Quan vam fer l'entrevista ens van rebres 4 persones de la xarxa:
- Vicent Galbis
- Alex
- Maria
- i Paco""",
            },
        ],
        "ratios": {
            "Persones remunerades": "0,1",
            "Càrrecs directius": "0,1",
            "Òrgans de govern": "",
            "Persones no remunerades": "",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """tenen un espai habilitat per a xiquets dins de la tenda per a quan venen amb els seus pares; ho tenen molt bén montat ;)""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """En el grup de consum, hi ha un 50-50% de socis/sòcies
no és un criteri que es tinga molt en compte; més bé el tenen interioritzat""",
            },
            "condicions-laborals": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "x",
                "p6": "x",
                "notas": """tenen una persona alliberada a temps parcial; la idea és - a més feina  i socis - passar a temps complet.""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Volen dur a assemblea lo de tindre una política de preus socials, és un tema pendent

actualment col·laboren amb la Camperola i Biotrèmol a Nivell proveïdors.""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """Utilitzen programari lliure per a gestionar les comandes (maistanet.com)""",
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "X",
                "p4": "x",
                "p5": "",
                "p6": "x",
                "notas": """setmanalment envien un email a la llista de sòcies per donar informació de nous productes, novetats, etc
a la fi de l'any fan un blanç econòmic públic""",
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Están en caixa popular tot i que estàn un poc decebuts per problemes tècnics que estan tenint.
Fiare la descarten per estar lluny. Realment el que necessiten és ingresar (per fer transferències) més que treure diners.""",
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """El local està adaptat a persones amb mobilitat reduïda
""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "X",
                "p5": "X",
                "p6": "x",
                "notas": """el tema de preus socials* volen reprendre'l en properes assemblees.""",
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "X",
                "p5": "",
                "p6": "X",
                "notas": """La xarxa està conformada per molts actors activistes d'alcoi; sempre ha estat un espai de confluència.
en el passat, quan estaven en el Partidor, donaven productes de la xarxa per al menjador pupular que hi havia al costat). Amb el canvi de local, açò s'ha deixat de banda.""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
        },
    },
    "fahrenheit-451-cafe-libros-alacant-alicante": {
        "editor": "raquelilla giner",
        "postal_code": "03012",
        "what_to_find": "",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "2,019",
        "comments": [{"date": "", "commenter": "", "text": """"""}],
        "ratios": {
            "Persones remunerades": "1,2",
            "Càrrecs directius": "",
            "Òrgans de govern": "",
            "Persones no remunerades": "",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "",
                "p1": "",
                "p2": "x",
                "p3": "",
                "p4": "",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "",
                "p2": "x",
                "p3": "x",
                "p4": "x",
                "p5": "x",
                "p6": "",
                "notas": """""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "x",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "condicions-laborals": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "X",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "x",
                "p2": "x",
                "p3": "X",
                "p4": "x",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "x",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """No utilizan Copy R pero lo indican en los contenidos de los documentos que publican. Dicen que son nulos a nivel informático. Utilizan Windows, Vision Win: que es un programa  de gestión de  librerias pero les obliga a trabajar con excel""",
            },
            "transparencia": {
                "no-aplica": "X",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """NO querían estar en un banco normal y priorizaron que fuera una cooperativa,están en caja de ingenieros""",
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "x",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Están abiertos a trabajar con colectivos vecinales, mov social, etc. cediendo el espacio para talleres,reuniones, etc.""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """Ayudan a la organización de la Mostra del llibre anakista""",
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Tienen libros en catalán, la web la están haciendo en valenciano, Tienen libros de contenido nacionismos y se quieren acercar al Tio Cuc (aunque están cautelosos)""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "x",
                "p2": "x",
                "p3": "x",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Utilizan bolsas de papel, tienen toalla en el baño, decoración de muebles reciclados, colaboran con la asociación R que R. Tienen cerveza Lluna y querían comprar en Mercatrèmol pero ahora no  les alcanza economicamente.""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "",
                "p4": "",
                "p5": "x",
                "p6": "",
                "notas": """""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Son de Iberdrola y no se plantean mucho""",
            },
        },
    },
    "cerveses-lluna-articultura-terra-coopv": {
        "editor": "pedro",
        "postal_code": "03801",
        "what_to_find": "",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "2,009",
        "comments": [{"date": "", "commenter": "", "text": """"""}],
        "ratios": {
            "Persones remunerades": "1,1",
            "Càrrecs directius": "1,1",
            "Òrgans de govern": "1,1",
            "Persones no remunerades": "",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "x",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "desenvolupament-personal": {
                "no-aplica": "x",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "x",
                "p6": "X",
                "notas": """Han peleado para que la asociación de cerveceros artesanos cambiase el nombre a cerveceras artesanas,  Maria ha recibido el premio dona artesana 2019""",
            },
            "condicions-laborals": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """Mas de la mitad de los indicadores no aplican por no tener trabajadorxs contratadas
Quizá sería un no aplica pero en caso de aplicar el criterio no debería penalizarse mucho por los criterios que no aplican""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "X",
                "p5": "",
                "p6": "X",
                "notas": """Hasta ahora han tenido que traer el lúpulo de Alemania pero han buscado un proyecto de Girona (biolupulus  -Albert-) que ha implementado todo el proceso de producción incluyendo el cultivo de la materia prima. Ahora les comprarán el producto a ellos y además están al tando de unas pruebas de cultivo que se están haciendo en Monover,  si funciona comprarán aquí el producto y lo llevarán a procesar a Girona,  Demuestran con ello el interés por hacer todo lo posible en obtener la mat prima de proximidad.  Tambien lo hacen con la cereza de temporada que la adquieren a través del grupo de consumo  de la Xarxa llauradora Alcoi excepto este año que la poca producción hizo el precio imposible y tuvieros que recurrir a otro proveedor también ecológico pero mayorista""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "X",
                "p5": "",
                "p6": "X",
                "notas": """Participan en diferentes colectivos que unen a productoras de cerveza artesana y ecológica, fomentando la ética y las buenas prácticas por ejemplo poniendo en evidencia la competencia desleal de las empresas más grandes""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """A pesar de que comentan que trabajan con un programa de licencia libre, no usan el sistema operativo linux y siguen con windows.  Creo que como todxs necesitan + formación e información """,
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "x",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """Han oido hablar del balance social però no lo han hecho, estuvieron en contacto con Reas cuando se intento formar un grupo en Alicante hace años però ahora están desconectadxs un poco por la voragime diaria del proyecto, família..  la 4 no aplicaría mucho..""",
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """Son socias de Coop 57 aunque nunca se han financiado con ellas,  Lo intentaron pero era complejo por el sistema de avales desde entidades de la ESS del territorio, ya que apenas hay. Han tenido que recurrir a banca  tradicional pero con valores como caixa d’0ntinient  caixa popular triodos (no están muy contentxs) y tienen cuenta en sabadell por cuestion meramente operativa ( a disgusto)
""",
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "x",
                "p2": "x",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """Hay alguno de los criterios que aplican poco por el hecho de ser una entidad pequeña sin trabajadorxs contratadas,  se han preocupado de que la barra que montan en eventos sea adaptada (con un parte más baja)  y han creado precedente""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "x",
                "p5": "X",
                "p6": "X",
                "notas": """Tienen una clara orientación a defender y divulgar valores de la ESS los contenidos en su web son muy explícitos en ese sentido
Colaboran con varios colectivos y causas: Fest,  Ecoaltea, Festival Boniatic,  Alacant desperta..""",
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "x",
                "p5": "x",
                "p6": "X",
                "notas": """Han hecho un gran esfuerzo por recuperar el espacio en el que están que es una antigua nave histórica de 1930 y tienen en mente proyectos con claro valor cultural.""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "",
                "p5": "X",
                "p6": "X",
                "notas": """Hay múltiples ejemplos de su vocación por trabajar desde el respeto al medio ambiente: vasos biodegradables en los eventos en los que participan (fueron de las primeras que los usaron) sus cervezas son ecológicas y veganas,  el proceso es a pequeña escala y artesano,  hacen esfuerzos para proveerse de productos ecológicos y de proximidad,  etc etc """,
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "x",
                "p4": "x",
                "p5": "",
                "p6": "X",
                "notas": """Realizan análisis de puntos de control crítico,  que es un proceso para evaluar la gestión de residuos, gestionan el residuo de cascarilla de forma que sea derivado para su uso en agroecología,  a escala doméstica practican el reciclaje convencional
""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "x",
                "p2": "X",
                "p3": "X",
                "p4": "x",
                "p5": "",
                "p6": "x",
                "notas": """No tienen calefacción  (damos fe)  participan en una coop tipo som energia  Enerco,  consumen energía de renobables planta fotovoltaica a traves de la coop Enerco,  tienen la iluminación Led de cuando hiciern la última inversión en 2016
""",
            },
        },
    },
    "rogle-coop-v-valencia": {
        "editor": "pedro",
        "postal_code": "46020",
        "what_to_find": "",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "2,014",
        "comments": [{"date": "", "commenter": "", "text": """"""}],
        "ratios": {
            "Persones remunerades": "4,4",
            "Càrrecs directius": "4,4",
            "Òrgans de govern": "4,4",
            "Persones no remunerades": "",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "X",
                "p4": "x",
                "p5": "x",
                "p6": "X",
                "notas": """Pedro: 4,  el único que no cumplen es el 5
Patricia: 4, pero yo les valoro a medias el punto 4 y el punto 5. El 4 porque no tienen protocolos escritos, y el 5 porque es cierto que no han hecho esa reflexión de forma colectiva y consciente, pero creo que se vio claramente que están en sintonía y con una opinión muy clara de los riesgos que suponen esos roles.
María:
  yo opino que son flojitos en el punto 2,y 4 ( no tienen metodología  concreta y la toma dedisiones tampoco está protocolizada o definidaba  mi parecer) El consenso no lo tienen protocolizado ni hay espacios asamblearios para destinados al abordaje de como se sienten etc,..
Yo daría un 3""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "x",
                "p5": "x",
                "p6": "x",
                "notas": """Pedro 3  o 4,  casi todos los procesos los hacen de forma natural, facilitan conciliación,  tienen un fondo para formación (obligatorio en coops pero supongo que superan es 5%)
Patricia 4. Para mi el único que no cumplen es el 3
María , 3. No existen mencanismos para la resolución de conflictos tratados o protocolizados, y me parecen todavía no tienen enfocado y tratado los mecanismos de desarrollo persona """,
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "x",
                "p2": "",
                "p3": "x",
                "p4": "x",
                "p5": "x",
                "p6": "X",
                "notas": """pedro 3  me ha dado la impresión que son un grupo de gente muy sensible y maduro aunque extrictamnte no cuenten con procedimientos predefinidos o no los necesiten
Patricia 3. Me gustó mucho que fueran conscientes de que les falta avanzar en esto, creo que eso ya es muy positivo.
María: 3""",
            },
            "condicions-laborals": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "x",
                "p5": "",
                "p6": "",
                "notas": """Pedro,  creo que es un caso de no aplica,  estan en fase de precariedad salarial pero son conscientes de ello y caminando hacia unos sueldos mejores
Patricia: para mi esto es un debate que tuvimos en mi cooperativa el otro día, quisiera discutirlo cuando nos reunamos. Para mi sí aplica, pero ¿Puede una empresa de la ESS alcanzar las condiciones salariales de una empresa capitalista? ¿Porque siempre  equiparamos sueldo digno a sueldo de empresa capitalista? Veo ahi un temazo. Yo les pondría un 3
María: Igual el punto 3 si que lo tienen bastante presente por su precariedad laboral.  Un  un punto..o como veáis. """,
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "X",
                "p5": "X",
                "p6": "",
                "notas": """Pedro 2 o 3 por los mismos motivos que en el criterio 3
Patricia: 3. Cumplen el 1, 4 y 5
María 3 No lo tienen como objetivo yo creo que por la falta de recursos 
""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "x",
                "p5": "",
                "p6": "",
                "notas": """Pedro 3 o 3,5  el uno y el dos lo cumplen y el tres parcialmente y el 4 creo que tambien
Patricia: Yo creo que cumplen el 3 del todo, ya que según contaron trabajan en temas de derecho a la vivienda y memoria histórica codo con codo con las entidades que pelean por esos temas. El 4 no tengo claro que lo cumplan. 3
María. 3,5""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "x",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Pedro: este quizá medio punto o nada
Patricia: Sí, porque hablaron de que todos sus materailes los comparten, pero no ponen nada de licencias, deberían mejorar en esto.
María : ok. """,
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "x",
                "p4": "x",
                "p5": "X",
                "p6": "X",
                "notas": """Pedro  4,  creoque aquí cumplen todos , algunos parcialmente 
Patricia, 4. Además tienen el blog bastante guay, contando lo que hacen y compartiendo información
María : 3,5 o 4 . Desde luego se intuye que ser transparentes es su objetivo. """,
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Pedro 3 o 3,5,  creo que si solo trabajan con triodos cumplen el 3, el 3 y parcialmente el 4
Patricia: 3. No les supone gran esfuerzo estar en triodos, ya un 3 me parece bastante.
María . 3. No es tanto un ojtetivo sino una uestión práctica y de facilidad""",
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "x",
                "p4": "",
                "p5": "x",
                "p6": "",
                "notas": """Pedro 1,  creo que solo cumplen el criterio 2
Patricia: Yo creo que de estos criterios solo cumplen claramente el 2 y un poco el 3, pero por la acción que realizan, creo que no es justo que tengan solo un 1. Les pondría el punto extra del comodín. 2
María; No me parece mal ponerle el 2. """,
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "x",
                "p6": "",
                "notas": """Pedrto 4,5 o 5   creo que aquí se salen
Patricia 4,5 en un tiempo llegarán al 5 seguro
María. Yo he valorado a priori 3.5 Pero teneis razon que es el objetivo o el traget de su cooperativa y me parece esenial valorarlo. Yo sólo veo que cumple o están comprometidos a tope con tres puntos. Pero bien. """,
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "X",
                "p6": "",
                "notas": """Pedro 4 o 4,5 cfreoq aquí tambien se salen
Patricia: 5 yo no me imagino ser mejor en este criterio, y ademś curran también con el ayto
María. No pude atender esta parte.""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Pedro, 2 o 2,5 creo que los tres primeros los cumplen parcialmente
Patricia: yo creo que el 2 no lo cumplen, no han hecho esa reflexión y creo que lo admitieron, simplemente van a comprar comida a 2 sitios concretos, pero creo que más por cooperar que por reflexión ambiental. 1
Yo opino que etán bastante flojitos en este punto. María. """,
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "x",
                "p2": "x",
                "p3": "x",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Pedro 1 o 1,5 creo que estan concienciadas y hacen lo que pueden pero no es una custión a la que presen demasiada atención,  tambin creo que quizá no aplica demasiado este criterio
Patricia Ok con Pedro
María 0k con ambos. bastante flojillos. """,
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "x",
                "p2": "x",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Pedro 2,  cumplen el 2 y el 3
Patricia: la luz es de origen renovable??? No lo escuché. Yo creo que cumplen el 3 y un poco el 1 por la reflexión de las ventanas. Len pondría un 2
María. 2/· por lo de la inversión en mejorar el aislamiento
""",
            },
        },
    },
    "paladar": {
        "editor": "raquelilla giner",
        "postal_code": "46020",
        "what_to_find": "",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "2,010",
        "comments": [{"date": "", "commenter": "", "text": """"""}],
        "ratios": {
            "Persones remunerades": "1,2",
            "Càrrecs directius": "",
            "Òrgans de govern": "2,1",
            "Persones no remunerades": "",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "x",
                "p4": "x",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "x",
                "p6": "",
                "notas": """""",
            },
            "condicions-laborals": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "X",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "X",
                "p5": "x",
                "p6": "",
                "notas": """""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "X",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "x",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """NO va fer per falta de temps i de coneiximents. Digen que estarien interesades en fer formacións per aprendrer-""",
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "X",
                "p4": "X",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Estan en Triodos per trebajen majoritariament amb Caixa Popular perquè troben a faltar serveis que necesiten""",
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "x",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "x",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "X",
                "p4": "x",
                "p5": "X",
                "p6": "",
                "notas": """El punto 2. se les pone 0.5 porque es cierto que priorizan la verdura local y agroecoológica per la carne no porque quieren mantener los precios asibles en el barrio""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "x",
                "p6": "",
                "notas": """""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "X",
                "p4": "X",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
        },
    },
    "antano-drogueria-ecosostenible": {
        "editor": "patricia",
        "postal_code": "",
        "what_to_find": "",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "",
        "comments": [{"date": "", "commenter": "", "text": """"""}],
        "ratios": {
            "Persones remunerades": "1,0",
            "Càrrecs directius": "",
            "Òrgans de govern": "",
            "Persones no remunerades": "",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "x",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "desenvolupament-personal": {
                "no-aplica": "x",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "condicions-laborals": {
                "no-aplica": "x",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """""",
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "x",
                "p2": "",
                "p3": "",
                "p4": "x",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
        },
    },
    "grup-consum-cabasset-gandia": {
        "editor": "tereseta",
        "postal_code": "46702",
        "what_to_find": "",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "2,017",
        "comments": [
            {
                "date": "2020-10-09 11:05:40",
                "commenter": "tereseta",
                "text": """-forma jurídica: cap
- col·laboracions: figa, bancal de Cento, teixint
- persones implicades: uns 33 membres
GENERE: 70% dones 30% homens

semestre passat 40 unitats de consum (setembre 2020)
5 euros/mes""",
            }
        ],
        "ratios": {
            "Persones remunerades": "",
            "Càrrecs directius": "",
            "Òrgans de govern": "",
            "Persones no remunerades": "",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """funcionament asambleari
4-6 anuals (mínim 2)
ASSEMBLEA SOBIRANA / voten però busquen el consens*** no definit
conflictes es debateixen MOLT

si tenen un document inicial: esborrany de futurs estatuts

GRUP DE GESTIO: gent + activa >> x avançar / + implicades en la gestió de grup

stma comunicació: wtsp 8 -10 persones""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """Fan vacances a l'agost per a descansar.
Grup de gestió controla si alguna persona no pot. Això és com un protocol,molt bé!

conflicte amb productor
*interns: no existeix res establert
>>> els primers anys van tindre conflicte per a preparar comandes; rondes volutàries (2 pers) >> van tardar però finalment
_ pujar quotes (via assemblea) >> punt conflicte (EIXIDA DE GENT)-- // sostenibilitat

- ronda cures asseblees NO
- formació coma tal, no, però solen fer reunió acollida nous membres / trobades germanor (menja conunt...) / van intentar fer una comissió... un cineforum/debat quan estaven en el local.

2 anys >>cabasset
ca saforaiu >> 3 mesos
centre social santa ana (confunament)
post confinament - bancal de cento (no te llum)
ara en octubre, canvien: acadèmia de Tino (espai de transformació social i personal: tallers extraescolars, cura-salut a través de cuina, alimentació eco, etc) espai social

- Fomenten el suport mutu
cures: amb la xica que ve de carrícola, amb els temps del productor... etc""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """llenguatge inclussiu, pero no com a entitat, sinó com a persones

s'han adherit a nivell personal a manifests,no com a entitat""",
            },
            "condicions-laborals": {
                "no-aplica": "x",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """des del començament, rrs de confiança
SPG
altres + xicotets que van i venen (xarxa llauradora, robert de marxuqueta, pere...)

tenien unes fitxes / visitar projectes
informalitat que va funcionant bé

en ultima assemblea, CONFLICTE amb productes de fora (farines) PLATANS* de canàries eco (sí, però amb % a productes eco que venen de fora)""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """Van fer una ronda de converses amb determinades organitzacions de consum, una d'elles mercatrèmol.

no ho tenen formalitzat, però estan prenent nota de cara a formar l'associació

RRS amb Mercatrèmol--
- Llucio del Banc de Terra (Xàtiva)

""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """programa de comandes: heredat d'altres grups com el d'alcoi, per inèrcia, molt bàsic però dona molts problemes
debat pendent

KATUMA""",
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """- debat sobre determinats productes
- grup de wasap obert
- no tenen canals de comunicació amb socis
- una persona s'encarrega de les XXSS (instagram, facebook)
tenen un acta setmanal on s'apunten els saldos, entregas, etc.s'envia després per wasap.""",
            },
            "finances-transformadores": {
                "no-aplica": "x",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """trasfons transformador
vertebració sobirania alimentari, terriori, productors, etc
jornades de portes obertes

quota: recuperar-ho amb feina* (gent que no te tans recursos)

col·laboració amb altres organitzacions: poquet****""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """Han fet algunes activitatsde sobirania alimentària, projeccions, etc, però fa temps que no en fan.

quan compartíen espai en el cabasset en teníen més contacte

tb molts membres, militen en altres col·lectius, està en el ADN del grup

ultim any, supervivència total***
Samarretes d'algun col·lectiu, calendaris de perl'horta... però promogut a nivell personal.""",
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """Col·laboren amb entitats locals (ca saforaui, figa, etc.) però de forma un poc passiva
Coneixen el seu territori però no estan molt implicats com a entitat en lluites locals.""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """recirculació de caixes a productors >> consumidors

aport de bosses de ràfia

durant confinament, company arreplegava tot, ho duia a lo de cento: OPTIMITZACIÓ DE TRAJECTES.

debat de  la proximitat

productes en bosses de paper, pots de vindre

RECICLATGE

no plàstics""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """teníen contenidors
GENEREN MÍNIM RESIDUS: matèria orgànica

reciclar***

trascendir?

està en la visió del nou espai social de transformació: taulò d'anuncis, per intercanviar OFERTA - DEMANDA

grup alegria i bons aliments* per a no carregar: part social >> grup wtsp // forment inter relació*** / tot i que de vergades, es generen debats i coses que no venen a cuento

es una ilussió""",
            },
            "consum-energetic": {
                "no-aplica": "X",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
        },
    },
    "tenda-mon": {
        "editor": "patricia",
        "postal_code": "46520",
        "what_to_find": "",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "1,998",
        "comments": [
            {
                "date": "2020-10-25 17:35:12",
                "commenter": "patricia",
                "text": """15-20 Persones remunerades
6 a la JD""",
            }
        ],
        "ratios": {
            "Persones remunerades": "1",
            "Càrrecs directius": "",
            "Òrgans de govern": "",
            "Persones no remunerades": "",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """""",
            },
            "condicions-laborals": {
                "no-aplica": "x",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "X",
                "p6": "X",
                "notas": """""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "X",
                "p5": "X",
                "p6": "X",
                "notas": """""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """""",
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """""",
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "X",
                "p6": "X",
                "notas": """""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "X",
                "p5": "",
                "p6": "X",
                "notas": """""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """""",
            },
        },
    },
    "doble-13-sant-vicent-raspeig-san-vicente-raspeig": {
        "editor": "Mar",
        "postal_code": "03690",
        "what_to_find": "",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "2,018",
        "comments": [{"date": "", "commenter": "", "text": """"""}],
        "ratios": {
            "Persones remunerades": "1,2",
            "Càrrecs directius": "0,2",
            "Òrgans de govern": "0,2",
            "Persones no remunerades": "",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "X",
                "p6": "",
                "notas": """Tienen áreas de decisión diferenciadas según sus especialidades, todas las demás decisiones son por consenso sin estructuras específicas, aun no han desarrollado RI están en un momento de consolidación, llevan poco tiempo, 1 o 2 años aprox""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """Están en un momento incipiente y no han tenido tiempo de estructurar el proyecto en este sentido,  si bien manifestaron no tener problemas con la flexibilidad y formación que cada uno precise""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """Algunos criterios los tocan pero no los cumplen claramente. Pero tienen intención de aprender y mejorar. Les falta formación y conocimiento sobre feminismo, sí tienen voluntad de trabajar en ese aspecto y aprender sobre el tema.""",
            },
            "condicions-laborals": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Aunque incipiente, sí tienen voluntad de trabajar positivamente en este aspecto.""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Se preocupan especialmente, y más en un sector tan complicado como es el de la pintura.""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Estan en un momento incipiente respecto a su conocimiento y conexión con la red de la ESS gracias a la entrevista han conocidos posibilidades de consumo sostenible con som energia , som comunicación, banca ética etc""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """No trabajan con licencias libres.""",
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Parece que no tenía conocimiento de la existencia de alternativas a la banca convencional y lo dañina que esta puede ser. """,
            },
            "cohesio-social": {
                "no-aplica": "x",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "X",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """están en conexión con el tejido local, barrios, etc""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """el proyecto está impregnado y motivado por un impulso de sensibilización socio-ambiental. Están preocupados por el residuo que generan, compran los tintes al mayor... teniendo en cuenta las características de su actividad). Son vegetarianos.""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Se preocupan y lo intentan integrar siempre que pueden, dentro de las dificultades de su sector.""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
        },
    },
    "paneko-elx-elche": {
        "editor": "Mar",
        "postal_code": "03204",
        "what_to_find": "",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "2,012",
        "comments": [{"date": "", "commenter": "", "text": """"""}],
        "ratios": {
            "Persones remunerades": "2,2",
            "Càrrecs directius": "1,1",
            "Òrgans de govern": "",
            "Persones no remunerades": "",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "x",
                "p5": "",
                "p6": "x",
                "notas": """Tienen áreas de decisión diferenciadas según sus especialidades, todas las demás decisiones son por consenso sin estructuras específicas, aun no han desarrollado RI están en un momento de consolidación, llevan poco tiempo, 1 o 2 años aprox""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """Están en un momento incipiente y no han tenido tiempo de estructurar el proyecto en este sentido,  si bien manifestaron no tener problemas con la flexibilidad y formación que cada uno precise""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """Algunos criterios los tocan pero no los cumplen claramente. Pero tienen intención de aprender y mejorar. Les falta formación y conocimiento sobre feminismo, sí tienen voluntad de trabajar en ese aspecto y aprender sobre el tema.""",
            },
            "condicions-laborals": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """Aunque incipiente, sí tienen voluntad de trabajar positivamente en este aspecto.""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """Se preocupan especialmente, y más en un sector tan complicado como es el de la pintura.""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """Estan en un momento incipiente respecto a su conocimiento y conexión con la red de la ESS gracias a la entrevista han conocidos posibilidades de consumo sostenible con som energia , som comunicación, banca ética etc""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """No trabajan con licencias libres.""",
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """""",
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """Parece que no tenía conocimiento de la existencia de alternativas a la banca convencional y lo dañina que esta puede ser. """,
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """""",
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "",
                "p2": "x",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """están en conexión con el tejido local, barrios, etc""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "X",
                "p6": "",
                "notas": """el proyecto está impregnado y motivado por un impulso de sensibilización socio-ambiental. Están preocupados por el residuo que generan, compran los tintes al mayor... teniendo en cuenta las características de su actividad). Son vegetarianos.""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "X",
                "p4": "x",
                "p5": "",
                "p6": "",
                "notas": """Se preocupan y lo intentan integrar siempre que pueden, dentro de las dificultades de su sector.""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """No cumplen el 2ª criterio, pero lo tienen en cuenta y se preocupan.""",
            },
        },
    },
    "naturalicia": {
        "editor": "Mar",
        "postal_code": "03292",
        "what_to_find": "",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "",
        "comments": [{"date": "", "commenter": "", "text": """"""}],
        "ratios": {
            "Persones remunerades": "1,0",
            "Càrrecs directius": "1,0",
            "Òrgans de govern": "",
            "Persones no remunerades": "",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "x",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Tienen áreas de decisión diferenciadas según sus especialidades, todas las demás decisiones son por consenso sin estructuras específicas, aun no han desarrollado RI están en un momento de consolidación, llevan poco tiempo, 1 o 2 años aprox""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """""",
            },
            "condicions-laborals": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "x",
                "p4": "x",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """""",
            },
            "cohesio-social": {
                "no-aplica": "x",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "",
                "p2": "x",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """""",
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """""",
            },
        },
    },
    "alternacoop": {
        "editor": "tereseta",
        "postal_code": "46135",
        "what_to_find": "",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "2,017",
        "comments": [
            {
                "date": "2020-11-07 17:23:46",
                "commenter": "tereseta",
                "text": """-forma jurídica: coop consumidors i usuari
des de gener 2017

423 socis a dat d'avui (novembre 2020)
*143 dones i resta hòmens""",
            }
        ],
        "ratios": {
            "Persones remunerades": "",
            "Càrrecs directius": "",
            "Òrgans de govern": "",
            "Persones no remunerades": "143,280",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """compten amb un equip tècnic reduït més operatiu* conformat per: rafa, mel, sacri, juan antonio, vicent

pressa decisions: assemblea

Conscients que han d'invertir + temps en comunicació i fer + esdeveniments perquè la gent s'anime / obrir a la participació. No troben molta gent disposta / no acaben de captar-la

No és un problema no es de tancament. Creuen que molta gent, al no tindre el servei prop / vehicles... donen suport però no s'involucren

DECISIONS DE PES, PROTOCOLS: en assemblea
*sobre-esforç""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "X",
                "p6": "X",
                "notas": """quan van començar servei: formació de sommobilitat
formació software utilització vehicles
han fet una formació de primer funcionament (prou autoformació buscant recursos)

Son molt flexibles. Es mouen per la motivació- SENSE ANIM DE LUCRE
molt online /// telegram, jitsi pressa decissions

de momento no han tingut conflictes amb socis / usuàries de vehicles
SOCIS EQUIP TECNIC: 4-5persones

ex. tema de preus ¡!
votació (per majoria, no per consens)""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "X",
                "p6": "X",
                "notas": """Son conscients que el genere femeni és més pro compartir, + practicitat
dels 40 usuaris, la meitat son dones...
en número d'associades 140 de 423 (el 33%)

Tot i així mancança de voluntàries / i de dones NO SABEM... son conscients

PERÒ sí que fan servir llenguantge inclussiu
anecdota nosotras - personas sòcias.""",
            },
            "condicions-laborals": {
                "no-aplica": "x",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Pedro,  creo que es un caso de no aplica,  estan en fase de precariedad salarial pero son conscientes de ello y caminando hacia unos sueldos mejores
Patricia: para mi esto es un debate que tuvimos en mi cooperativa el otro día, quisiera discutirlo cuando nos reunamos. Para mi sí aplica, pero ¿Puede una empresa de la ESS alcanzar las condiciones salariales de una empresa capitalista? ¿Porque siempre  equiparamos sueldo digno a sueldo de empresa capitalista? Veo ahi un temazo. Yo les pondría un 3
María: Igual el punto 3 si que lo tienen bastante presente por su precariedad laboral.  Un  un punto..o como veáis. """,
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "X",
                "notas": """app proveidor: van apostar per l'app de la coop europea (servei car share)

Tenen els segurs en ARÇ
Els telèfons en eticom / som connexio
La llum en som energia

FAIRPHONE

ESS sempre busquen d'ahi
a lo local: ultima compra ferreteria (pany)
vinils en el poble""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "X",
                "notas": """Son la primera comunitat energètica de l'estat espanyol FETA REALITAT
Juan amb sapiens energia coop (fa 3 mesos, en Canet)
sostre del coworking d'alternacoop""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "X",
                "p6": "X",
                "notas": """Tracten de fomentar l'ús de programari lliure
valors de la coop

software de facturacio: programari lliure (reutilitzat d'un soci de la coop)
software coops: MOBILITY FACTORY - android

app. google maps >>> van a migrar-ho a OSM""",
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """Fan un correu de benvinguda però no comparteixen els estatuts (diuen que no li donen importància)

organigrama: HO TENEN PENDENT incloure en la web

econòmic: CAIXA POPULAR per la domiciliació de rebuts
de moment s'autofinancien amb capital social""",
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "",
                "p4": "X",
                "p5": "",
                "p6": "X",
                "notas": """de moment, fan servir Caixa popular -  compte operatiu, del dia a dia
arç en assegurança

tenen sòcies de FIARE / de moment no ho necessiten""",
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "X",
                "p6": "X",
                "notas": """Tenen un soci que treballa amb persones en risc, vulnerabilitat i ha proposat  externalitzar neteja*** (però de moment no ho han fet però més per un tema de sostenibilitat)


tot i així, ho tenen contemplat""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "X",
                "p5": "X",
                "p6": "X",
                "notas": """pera cada vehicle que fiquen al carrer - lleven a 10 del carrer
MILLORA MEDIAMBIENTAL - salut

apostem pel decreixement
VEHICLE COMPARTIT
no comprar vehicles... reciclatge bateries autoconsum

com a coop no estan en cap xarxa, están més a nivell personal
van de la ma de sommobilitat

ECOMACLET*** participació
REAS (EN ?)""",
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "X",
                "p5": "X",
                "p6": "X",
                "notas": """tenen els estatuts en valencià i escriptures de constitució total en valencià.
web biligüe

estan en contacte amb HORTA NORD, i fent tractes amb els ajuntament per tal d'oferir-los vehicles elèctrics
tb s'intenta descentralitzar >> castelló""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "X",
                "p5": "X",
                "p6": "X",
                "notas": """a la presentació sempre diuen:
-no tingues vehicle, ves caminant, o en bici... i si no.... VEHICLE ELECTRIC COMPARTIT

van fer una Jornada en Albalat + presentació vehicle + jornada sensibilització""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "x",
                "p5": "X",
                "p6": "X",
                "notas": """per cada vehicle elèctric, lleven 10 de combustió del carrer
- reducció de CO2 (1 electris x 10 combustio)
- residu: NO GENEREN

Van timdre un problema amb una bateria de l'EVALIA i NISSAN la va canviar. Saben hi ha un mercat segona ma, autoconsum (tema bateries)""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "X",
                "p6": "X",
                "notas": """SOM ENERGIA - comtracte llum
autoconsum
2on projecte: comunitat energètica local, a compartir amb la resta del veïnat""",
            },
        },
    },
    "formigues-liles": {
        "editor": "pedro",
        "postal_code": "46008",
        "what_to_find": "",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "2,018",
        "comments": [{"date": "", "commenter": "", "text": """"""}],
        "ratios": {
            "Persones remunerades": "3,0",
            "Càrrecs directius": "3,0",
            "Òrgans de govern": "3,0",
            "Persones no remunerades": "",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "x",
                "p5": "X",
                "p6": "X",
                "notas": """justament es de dediquen a això
document d'estructura que ara mateix etsán revissant: pressa decisions, més tècnics, logístics
espai emocional: reben facilitació / curro tema rols

estan facilitant-se proces pressa decissió / AJUDA EXTERNA
aprenent a barrallar-nos""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "X",
                "p6": "X",
                "notas": """Tienen claro que es mejor renunciar a un curro si implica un desequilibrio
post grado Nuria pagado por la coop y otras formaciones estratégicas
molt imp les vides de cadascuna. SOSTENEN
ACTIVITATS EXTRAESCOLARS / miren horaris
formacions que van: les paga la coop si son estratègiques.

cora (perra)/ eva*""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "X",
                "p6": "X",
                "notas": """pla d'igualdat que no han fet per mandra""",
            },
            "condicions-laborals": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "x",
                "p6": "",
                "notas": """todas el mismo sueldo. salario mínimo interprofesional
les vale para mantenerse, hacer pagos.
momentos muy jodidos
lo de cobrar el ERTE les está viniendo MUY bien
a nivel de gasto es tiempo y kilometraje (esta es la deuda maxima)

cada mes depende, varia mucho. CURRO MOLT INESTABLE
curs escolar*
(estiu, vacances no tenen curro)
NOVEMBRE I MARÇ: pico***** Curren molt

*curro asalariat (ajuntaments)
*curro no remunerat (de despatx, comunicació, xarxes)... que no dona pasta però s'han de fer. REPARTEIXEN ROLS.. Però estan explicant molt bé: gestió del coneixement (TUTORIALS)
*curro militant

REPARTIT*

que cadascuna escrivira tot el que voldria fer DURANT UN MES sense problemes de pasta, sense límits.
- menjar eco
- viure en una casa x
-viajar
- molts dies volem currar
treballar 3 dies a la setmanam guanyant el doble, sense estar mega explotada eixos díes

tenen una MITJANA: referent

LIMITS******

desde que son coop, a finals d'anys sempre están ebenefici: tot i que els diners arriben tard. matalàs de pasta... amb administració

solidaritat entre elles
tema subvenció: mandra""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "x",
                "p5": "x",
                "p6": "x",
                "notas": """tenen poc gasto material
tiendecita barrio*

contractacions: disseny web dona precaria*
disseny: fotos, materials gràfics...

gestoria: coop

COOP VEGANA: materials, espais, etc
""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "x",
                "p4": "x",
                "p5": "",
                "p6": "X",
                "notas": """contrataciones gestoria web a personas del entorno con necesidades y afinidad política,  trabajan por una xarxa de cooperativas
ESS - economia alternativa anticapitalista
interés en treball en xarxa

xiques que fan el mateix q elles
contactar-les CALANDRA**** (veure web)
cris i javi, facilitació
""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "x",
                "p2": "",
                "p3": "x",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """van poc a poc
zoom* experiència negativa amb jitsi
xarxes socials privatives

DESCONEIXEMENT
altres prioritats

ELS COSTA MOLT

cc
nextcloud""",
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "x",
                "p2": "x",
                "p3": "X",
                "p4": "X",
                "p5": "x",
                "p6": "",
                "notas": """externa
materials desorganitzats / no estan donant prioritats
no fan informalment pero no formalment

ACORDS: entrades del blog

actes, reunions: inventen* parlen i es reuneixen més del que diu la llei.

* regalemnt intern: el seu document""",
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """No se han planteado aun el tema finanzas éticas
caixa popular.
no sirveixen caixes online.. han d'anar en persona. SUCURSAL

al ppi santander***
""",
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """taller gratuits (contratados por ayuntamientos)
accesibilitat

PERSONA SORDA -reflexió
FESOC*

preus diferents segons entitats
GRATUITAT

HORARIOS*** dones, accessibilitat
poden dur xiquets, o la dona a la que cuida.
""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "X",
                "p5": "",
                "p6": "x",
                "notas": """estatutos en femenino tuvieron problemas
colaboran con movimientos por la transformación social : reas

web: qui som. per què coop, ess,

LUCRO* sous.

vincles a nivell personal: cuidem beni, assemblea femi, l'horta, ateneu xativa, conca, etc
""",
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "x",
                "p5": "",
                "p6": "X",
                "notas": """idioma
MOLT vinculades amb altres iniciatives defensa territ / barrial
a nivell personal/coop
asseorament telefònic: agressions, feminismes (particulars)

ALTRA MANERA DE RR

portar el discurs del mvt social a pobles... a dispossició.

ESTATUTS: projectes a ajuntaments. RADICALITAT
militància, carrer""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """ecològic, vegan
estan en un lloc on no poden escollir...
es accesible

PIZARRA para evitar papel...
reciclar""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "x",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """impressora* nova / punt net cartutxos
mòbils reciclat
PLASTIFACIÓ***
eddings recarreblaes
""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "x",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Compartixen cotxe - vehicñes partic
ordinadors personals***

transport public inviable
""",
            },
        },
    },
    "espais-conviure-coop-v": {
        "editor": "pedro",
        "postal_code": "46022",
        "what_to_find": "",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "2,015",
        "comments": [{"date": "", "commenter": "", "text": """"""}],
        "ratios": {
            "Persones remunerades": "2,1",
            "Càrrecs directius": "1,1",
            "Òrgans de govern": "1,1",
            "Persones no remunerades": "",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "x",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """justament es de dediquen a això
document d'estructura que ara mateix etsán revissant: pressa decisions, més tècnics, logístics
espai emocional: reben facilitació / curro tema rols

estan facilitant-se proces pressa decissió / AJUDA EXTERNA
aprenent a barrallar-nos""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """Pedro: (2,5P) Han realizado un plan de igualdad y tienen paralelamente un plan de formación. Considero que cumplen el 1 y el 2,  el 3,4,5 aplican poco por el mismo motivo que el criterio anterior no obstante por la política abierta de la entidad añadiría medio punto
Rebeca:  conciliación pactada""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "x",
                "p4": "X",
                "p5": "",
                "p6": "x",
                "notas": """Pedro: (3P) Han realizado un plan de igualdad por lo que cumple el 1 parte del 2,3,4,y 5
Rebeca: cumplen el 4 """,
            },
            "condicions-laborals": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "x",
                "p4": "",
                "p5": "X",
                "p6": "",
                "notas": """Pedro: (3P) Cumplirían el 1, parcialmente el 2 y el 3 (tienen una persona colaboradora contratada) y parcialmente el 5
Rebeca cumplen el 5""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "",
                "p4": "x",
                "p5": "",
                "p6": "",
                "notas": """Pedro: (2P) cumplen el 1 (ejm ascensores empresas locales y no multinacionales) la 2 en parte aunque no llevan un control exahustivo.  La 4 en parte
Rebeca: cumplen el 1 """,
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """Pedro: (2P)cumple la 1 alguna vez trabajan con Taulell que eran de reas y AEIOluz tambien tienen convenio con asesoría coop,  cumple la 3 en su relación con otros agentes para proponer espacios verdes en terrazas y de generación de energías limpias
Rebeca:  cumplen el 3""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Pedro: (0P- no cumple este criterio) Aqui casi nada,  solo usan algunos programas libres como libre office
Rebeca: no cumplen""",
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "X",
                "p6": "X",
                "notas": """Pedro: (4P) cumpliría en buena parte la 2 la 3 y la 5, por hacer el balance social y Tornallom les daría un punto más
Rebeca: cumplen el 4
""",
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """Pedro: (3P) trabajan con Triodos y por operatividad con caixa popular que es coop y bastante próxima a los valores de la banca ética.  Promueven entre sus comunidades de propietarias clientas que trabajen con estas entidades, cumplirían 1,2 y 1 punto más por la divulgación e influencia que hacen
Rebeca: cumplen el 2""",
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """Pedro: (2P) Han hecho plan igualdad, no atienden al público directamente, tienen la web bilingüe en valenciano también. Parcialmente la 1 y por el plan de igualdad le daría otro
Rebeca: Algunos criterios los quieren tener en cuenta en el futuro pero de momento no cumplen
""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "x",
                "p2": "X",
                "p3": "",
                "p4": "X",
                "p5": "",
                "p6": "X",
                "notas": """Pedro: (2,5P) A nivel particular son personas comprometidas y participan en activismos sociales (radio clara) Son conscientes y actuan frente a la corrupción sistémica en su gremio, promueven pequeños cambios en las C. prop que administran con proveedores y productos más sostenibles, banca con coop, con som energía… Parcialmente un poco de todas..
Rebeca: cumplen el 2
correción despues de valorar contenidos con discurso transformador y su participación en Reas añado un punto má """,
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Pedro: (1,5P) web tambien en valenciano, (no son valencianoparlantes) su propio nombre es un guiño a la cultura local, cumplirían parcialmente la 1, 2,3
Rebeca: cumplen el 2
consenso 2,5""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "x",
                "p2": "x",
                "p3": "x",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """Pedro: (1,5P) a nivel personal consumen en ecológico de proximidad,  som alimentació, usan la bici para desplazarse y un vehichulo en renting para desplazamientos largos. Promueven entre sus comunidades clientes la contratación de energías limpias y ellos consumen papel ecologico,  cumplirían 1,2,3 parcialmente
Rebeca: a nivel personal si cumplen y debería considerarse
consenso 2p""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "x",
                "p2": "",
                "p3": "x",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Pedro: (1P) Apuestan por eliminar el papel en todas sus gestiones, se plantean asesorar a sus clientes sobre fórmulas de de gestion y minimización de residuos , parcialmente 1 y 3
Rebeca: de momento no cumplen,  se lo están empezando a plantear a través de esta entrevista""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "x",
                "p2": "x",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """Pedro: (2P) trabajan con som enengia,  apuestan por introducir a esta coop como poveedora de sus com. De prop clientas,  cooperan con AEIOluz para estudios divulgación etc sobre el tema de reducir consumos etc cumple la 3 y parcialmente 1 y 2
Rebeca: cumplen el 3, tienen som energia a nivel particular y la intentan promover""",
            },
        },
    },
    "cotasa": {
        "editor": "Mar",
        "postal_code": "46800",
        "what_to_find": "",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "2,016",
        "comments": [{"date": "", "commenter": "", "text": """"""}],
        "ratios": {
            "Persones remunerades": "1,2",
            "Càrrecs directius": "1,2",
            "Òrgans de govern": "1,2",
            "Persones no remunerades": "",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "condicions-laborals": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "X",
                "p4": "X",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "x",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "x",
                "p6": "",
                "notas": """""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
        },
    },
    "repartidora": {
        "editor": "raquelilla giner",
        "postal_code": "46020",
        "what_to_find": "",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "2,014",
        "comments": [{"date": "", "commenter": "", "text": """"""}],
        "ratios": {
            "Persones remunerades": "1,1",
            "Càrrecs directius": "1,1",
            "Òrgans de govern": "",
            "Persones no remunerades": "1,1",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "x",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "x",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "X",
                "p4": "X",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "condicions-laborals": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "x",
                "p5": "X",
                "p6": "",
                "notas": """Esta és la puntuació 'per estructures xicotetes'""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "x",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "X",
                "p4": "x",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "x",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """NO utilitzen per desconeiximent i falta de temps per investigar però voldrien rebre formació per fer el camvi.""",
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """Tata l'iformació està  a la web i els comptes són totalment compartides amb les socies. """,
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "X",
                "p6": "",
                "notas": """Tenen Caixa Popular per recomanació d'un amic però no s'han apropat mai a la banca ètica per desconeiximent. Encara que estan molt interessades. Consideren necessàries formacions internes dintre de les associancions """,
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "X",
                "p6": "",
                "notas": """Reconeixen que per entrar una persona a trabajar ha de tindre uns coneiximents de l'àrea prou grans i que parlar valencià seria un requisit fonamental i ven difícil l'entrada a persones de exclusió social sense estos requisits.

'Depenent de les tasques i les necessitats seria un factor limitant'""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "X",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "x",
                "p5": "X",
                "p6": "",
                "notas": """""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "",
                "p4": "X",
                "p5": "X",
                "p6": "",
                "notas": """Coses que fan: tindre contingut relacionat amb el canvi climàtic, sistema capitalista ,etc./ tenen leds/ Cooperen amb Coop. Teneno (Ontinyent)/ En un futur estan pensant en fer un apartat de llibres de segona ma i prèstec/ No promouen el material reciclat o perquè ho veuen difícil. / NO s'han plantejar fer repartiment de llibres o encomandes amb transport bici, ecològics o més ostenible.""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "X",
                "p6": "x",
                "notas": """El s residus que tenen ho reciclen/ reutilitzen folios/ intentenno imprimir però no treballen fer els llibres en paper ecològic en l'editorial Caliu, no hi ha una preferència per editorials o llibres que sigan amb paper reciclat, n o han pensat en fer el repartiment o distribució del llubres de manera més sostenible.""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "X",
                "p2": "",
                "p3": "",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """Utilitzen leds però no són de Som Energia, """,
            },
        },
    },
    "cuinatur-cuina-natural-castello-plana-castellon-plana": {
        "editor": "raquelilla giner",
        "postal_code": "12005",
        "what_to_find": "",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "2,004",
        "comments": [{"date": "", "commenter": "", "text": """"""}],
        "ratios": {
            "Persones remunerades": "",
            "Càrrecs directius": "",
            "Òrgans de govern": "",
            "Persones no remunerades": "",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "",
                "p1": "",
                "p2": "x",
                "p3": "x",
                "p4": "",
                "p5": "",
                "p6": "x",
                "notas": """-NO tenen dinàmiques de preses de decisions
- 3 Persones s'encarrguen d'avualr el content de les i els trablladores/rs
-Fan un taller d'educació emocional ambles monitores perquè ho apliquen amb els xiquets
-4 persones de direcció prenen les decions/ 3 persones Proveïdors, 2 a nivell d'estratègia

Esprou piramidal l'empresa encara que intenten que les seues treballadores estiguen bé.""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "x",
                "p4": "X",
                "p5": "",
                "p6": "x",
                "notas": """-És difícil treballar de manera d'equip perquè les treballadores estan d'abans d'arribar i es sent part del centre i n o de l'empresa.
-Abans feien 3 reunions anuals per grups laboras (per facilitar la logística. Anaen a Roqueta/Pou de Beca
-A nivell monitores no ho fan perquè no és possible fer-ho amb tanta gent""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "",
                "p2": "x",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """-tenen un Pla d'gualtat
-Hi ha més dones que homens en els puestos de responsabilitat i presa de decisions""",
            },
            "condicions-laborals": {
                "no-aplica": "",
                "p1": "",
                "p2": "X",
                "p3": "",
                "p4": "x",
                "p5": "x",
                "p6": "",
                "notas": """-La ratio és menos del doble
-Les monitores de Castellò cobren igual que les de València on el conveni cobren més alt.
-NO hi ha sindicat en l'empresa encra ue li agradaria però al mateix temps li fa una mica de por.""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "X",
                "p4": "x",
                "p5": "x",
                "p6": "x",
                "notas": """-COmpra productors, locals, ecològics i de temporada.
-No confia en els productors que no tingan la filosofia.""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "x",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """3. A Marta Feliu sí que le asesoró y le ayudó a la hora de diseñar los menus de Cuina VIva
-Proveidors amb referencies de a prop i agroecològics
-NO mira que estiga en xarxes socials
""",
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """-Encara que tenen windows el servidor és de programador lliure i desenvolupament intern ho ha fet un treballador de NovaTerra
-NO és preioritari perquè és un risc""",
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "",
                "p5": "X",
                "p6": "",
                "notas": """-Els contes són obligatòries al ser una S.L.
-Fan el Balanç del Bé comú
-VAn a animar a que tots els seus prveïdors facen el BS

Són uns convençut del BS i creu que és real i útil i important de fer""",
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "x",
                "p6": "x",
                "notas": """Estan al la EBCpor convicció
-Van intentar treballar amb Triodos i Fiare diverses vegades i finalment no van poder treballar amb ells èrquè no tenen servei.
-Tenen Caixa Pupular per filosofia però li costa moltíssim i la resta del 50% ho tenen Sabadell/ caixa Mar i Caixa Popular.

-Ho intentaria de niu amb la banca ética
-Coneix coop 57 però li falta informació""",
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "X",
                "p5": "x",
                "p6": "",
                "notas": """Treballen el 50% amb TerraNova que és una funadació de integració laboral i La Crisopa que treballa amb gent inmigrant.
-A l'oficina és parla de tot castellà i valnecià però les monitores volem que parlen valencià perque vans als col.legis.
-Intenten insertar a gent amb diversitat múltiple però depend del cole.""",
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "X",
                "p5": "",
                "p6": "X",
                "notas": """-Felix de CuiNatur està molt implicat i participa activament articulant-se en diferentes moviments social com Escoles que alimenten (que participa amb Cerai i Justicia aliemntària) Slow food, està sempre disposat a participar en xarrades, él discurs que diu sembla que és una filosofia de veres encara que tienegues coses de empresa ordinaria com que és molt piramidal.

""",
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "x",
                "p4": "",
                "p5": "X",
                "p6": "",
                "notas": """-Valencià és l'idioma preioritari però no és excluyent
-S'articula en un grup de treball per modificar el sistema econòmic mitjançant el comsum responsable de la compra pública verda amb 'Escoles que alimenten'

-Fa gastranomia valenciana tradicional perquè creu que és ho que s'ha de fer per aliemntar bé als xiquets i part de la nostra cultura.""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "x",
                "p5": "x",
                "p6": "",
                "notas": """-Compren local
-Els cotxes van amb gas
-Treballen amb Som Energia
-Els envasos són de 5quilo per facilitar el treball a les treballadores i són de cartó.
-Tenen pendent treballar el balafilament alimentari
-VAn intentar fer un treball de compostage però no li va ixir però volen intentar-ho de nou""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "x",
                "p2": "",
                "p3": "X",
                "p4": "X",
                "p5": "X",
                "p6": "x",
                "notas": """""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "X",
                "p4": "X",
                "p5": "",
                "p6": "",
                "notas": """-Son de Som Energia
-Tenen cotxes de gas
-Utilitzen leds

En seguida es van fer de Som Energia i van intentar que els seus proveïdors també ho ferem peró no van funcionar""",
            },
        },
    },
    "restaurante-crudivegano-momo": {
        "editor": "pedro",
        "postal_code": "03004",
        "what_to_find": "Comida natural ecológica crudivegana",
        "other_networks": "",
        "anual_invoicing": "",
        "foundation_year": "2019",
        "comments": [],
        "ratios": {
            "Persones remunerades": "1,0",
            "Càrrecs directius": "1,0",
            "Òrgans de govern": "1,0",
            "Persones no remunerades": "",
        },
        "criterias": {
            "democracia-interna": {
                "no-aplica": "x",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "desenvolupament-personal": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """PEDRO :___________No aplica///// 1

RAQUEL:________  1 punto la base del proyecto está desde los cuidados por detrás de la rentabilidad

EVA:______________1 PUNT""",
            },
            "perspectiva-feminista": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """PEDRO :___________No cumple/ PUEDO ENTENDER ARGUMENTOS A FAVOR DE 1 PUNTO.

RAQUEL:__________1punto,  argumentos similares a evA

EVA:_______________ Si bien el proyecto es de tipo familiar y los órganos y esferas de decisión se remiten a la pareja, en la decisión del reparto de tareas, se hizo según las habilidades de cada una, lo cual resultó en que fue él quien se quedó al cuidado de los hijos y ella quien se quedó regentando el negocio presencialmente. Esto, creo, muestra al menos, parte de dinámicas feministas, ya que rompe con los roles patriarcales predeterminados; además, elige(n) su horario de trabajo priorizando los cuidados familiares. Solo por eso le pondría 1 punto. """,
            },
            "condicions-laborals": {
                "no-aplica": "x",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """""",
            },
            "proveidores": {
                "no-aplica": "",
                "p1": "x",
                "p2": "x",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """PEDRO :___________cumple el 1, el 2 a medias, la 3 un poco, la 4 un poco (compra en mercatremol) todo el producto es no procesado, solo las materias primas que no hay aquí las trae de afuera, 2

RAQUEL:________₁   ACORDAMOS 2

EVA:_______________ La compra de lo principal en Mercatrèmol habla de la importancia que le dan a un producto bueno y justo en varios sentidos. No obstante, creo que su trabajo en este bloque se limita a eso. (No hablamos de bebidas ni de vajilla, mobiliario y esas cosas!) 2 puntos?""",
            },
            "intercooperacio": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """PEDRO :___________no sabe lo que es la ESS ni está vinculada a redes,  creo que no cumple sin embargo tiene claro no trabajar la comida a domicilio por la perversión de las empresas que dominan este mercado, globo, etc  le pondría 1 punto solo por eso

RAQUEL:__________1

EVA:_______________Como Pedro indica, pese a no conocer el marco de la ESS, entre la compra de frescos a Mercatrèmol, que sí forma parte de ésta y los precios asequibles, le pondría un 1. """,
            },
            "llicencies-lliures": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """PEDRO :___________No cumple

RAQUEL:__________NO

EVA:_______________ No cumple
""",
            },
            "transparencia": {
                "no-aplica": "",
                "p1": "x",
                "p2": "x",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """PEDRO :___________ Cumple 1 y 2 ,  3 y 4 no aplican, 5 muestra interés. Pondría un 2 o 3 por buenas prácticas en el esfuerzo de explicar por norma al detalle a sus clientes las características del producto.

RAQUEL:_________1,,     acepta 2

EVA:_______________  De acuerdo con Pedro. 2 Claros otros no aplica
""",
            },
            "finances-transformadores": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """PEDRO :___________No cumple pero muestra interés en conocer. Tuvo cuenta en Triodos y la quito por problemas operativos pero está pensando en alguna alternativa, ahora trabaja con Sabadell

RAQUEL:__________no

EVA:_______________ Como dice Pedro.
""",
            },
            "cohesio-social": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """PEDRO :___________el tema del no glúten hace que todas las personas alérgicas puedan encontrar un sitio sin ese riesgo,  cumple los estándares de accesibilidad en el local, ayudan constantemente a la gente no puesta a entender la carta. Le pondría 1 punto a pesar de no cumplir claramente ningún indicador

RAQUEL:_________ 1 punto

EVA:_______________ La ausencia de glúten, pero también de productos animales, cocinados e incluso que todo lo fresco sea ecológico, lo convierte en una propuesta para la minoría que quiere o necesita ceñirse a eso. Como dice Pedro, el local es accesible y ponen una gran atención en cada cliente. 1 punto me parece justo. """,
            },
            "transformacio-social": {
                "no-aplica": "",
                "p1": "x",
                "p2": "",
                "p3": "x",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """PEDRO :___________A pesar de no cumplir claramente con ninguno de los indicadores el modelo de gestión sin afán de lucro, pequeño negocio para ganarse la vida ofreciendo un servicio de alimentación saludable y coherente con la sostenibilidad (no carne) (producto ecológico) a precio razonable, hace a mi juicio que sea un proyecto que aporta a la transformación hacia un orden social más justo y sostenible. Por ello de pondría 2 puntos

RAQUEL:__________2p

EVA:_______________ Coincido con Pedro. Aunque no se lo planteen con la profundidad política con la que nosotras lo planteamos, de facto, llevan a cabo una iniciativa contrapuesta a la empresa capitalista y, por tanto, contribuyen a un modelo transformador. 2 puntos por ello """,
            },
            "arrelament-territorial": {
                "no-aplica": "",
                "p1": "",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "",
                "notas": """PEDRO :___________No cumple

RAQUEL:__________no

EVA:_______________ No cumple.""",
            },
            "sostenibilitat-ambiental": {
                "no-aplica": "",
                "p1": "X",
                "p2": "X",
                "p3": "",
                "p4": "",
                "p5": "x",
                "p6": "x",
                "notas": """PEDRO :___________cumpliría el 1 y el 2 y el 3 en parte indirectamente tocaría el 5 le pondría 3 puntos

RAQUEL:__________3

EVA:_______________ Ídem que Pedro. 3""",
            },
            "gestio-de-residus": {
                "no-aplica": "",
                "p1": "X",
                "p2": "x",
                "p3": "",
                "p4": "x",
                "p5": "",
                "p6": "X",
                "notas": """PEDRO :___________cumple el 1 el 2 en parte y quizá el 3 le pondría 2 puntos// 3 x buenas prácticas

RAQUEL:__________3 x buenas prácticas

EVA:_______________ Diría que cumple el 1 y el 3 o 5, en el sentido de que les sobra muy poco producto y éste se lo llevan a casa. Diría que hay algún tipo de cálculo para esa minimización de sobras. No habló sobre reutilización o reparación de aparatos. También le pondría 2 puntos. 3 x buenas prácticas""",
            },
            "consum-energetic": {
                "no-aplica": "",
                "p1": "x",
                "p2": "",
                "p3": "",
                "p4": "",
                "p5": "",
                "p6": "X",
                "notas": """PEDRO :___________muestra interés por compañias eticas, gasta poquisima energía en el proceso de preparación de los alimentos le pondría un 1

RAQUEL:_________1 p

EVA:_______________ Opino igual que Pedro solo que me parece muy innovadora una cocina de restaurante con tan ínfimo uso energético, por lo que le pondría 2 puntos.
 consenso media 1,5""",
            },
        },
    },
}
