# from itertools import chain, combinations
# import factory

# pip install google_trans_new
from google_trans_new import google_translator


from random import randint

from django.core.exceptions import ObjectDoesNotExist
from django.core.management import call_command
from django.core.management.base import BaseCommand, CommandError
import requests
from pprint import pprint
from django.contrib.gis.geos import Point

# from networkapi.people.models import InternetHealthIssue
from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType

superaixadetes_group, _ = Group.objects.get_or_create(name="SuperAixadetes")
aixadetes_group, _ = Group.objects.get_or_create(name="Aixadetes")
from django.utils.text import slugify

# Factories

from mapPointCriterias.models import (
    Category,
    CategoryTranslation,
    BurocracyCategory,
    LegalForm,
    ParticipantRole,
    ParticipantRoleForMapPoint,
    Criteria,
    CriteriaTranslation,
    Comment,
    MapPoint,
    MapPointTranslation,
    Network,
    Community,
    # PointStatus,
    LandLevel_1,
    LandLevel_2,
    LandLevel_3,
    PointLink,
    LinkType,
    TrinaryBoolean,
    EvaluationCriteriasForMapPoint,
    UserProfile,
)
import mapPointCriterias.initial_data as initial_data

translator = google_translator()


def get_translation(phrase):
    if phrase.strip() in initial_data.TRANSLATIONS:
        return initial_data.TRANSLATIONS[phrase.strip()]
    else:

        if phrase.strip():
            # print(f"Not found translation for '{phrase.strip()}'")
            translated_phrase = translator.translate(
                phrase, lang_tgt="es", lang_src="ca"
            ).capitalize()
            print(f'"""{phrase.strip()}""":"""{translated_phrase.strip()}""",')
            return translated_phrase
        else:
            return None


def get_territories():
    # Get each country levels from https://wiki.openstreetmap.org/wiki/Tag:boundary%3Dadministrative
    # Get the overpass request: http://overpass-turbo.eu/s/10KE
    # [out:json][timeout:25];
    # {{geocodeArea:El Valle de Cofrentes-Ayora}}->.searchArea;
    # // gather results
    # (
    #   // query part for: “admin_level=10”
    #   node["admin_level"="8"](area.searchArea);
    #   way["admin_level"="8"](area.searchArea);
    #   relation["admin_level"="8"](area.searchArea);
    # );
    # // print results
    # out meta;
    # >;
    # out meta;

    # For valencia it was taken easily from an official .xls http://www.pegv.gva.es/es/muncom  converted to ods.
    territories_ods = (
        "/home/alberto/workspace/otrosproyectos/tornallom/django/municipios_CV.ods"
    )
    from pyexcel_ods import get_data
    import json

    print("Opening", territories_ods)
    data = get_data(territories_ods)

    # pprint(data["Totes"])
    first_sheetname = list(data.keys())[0]
    sheet = data[first_sheetname]
    points = []
    from collections import defaultdict

    provincias = defaultdict(lambda: defaultdict(lambda: defaultdict(str)))

    for line in sheet[1:]:
        cols = len(line)
        if cols == 4:
            provincia_name = line[0].strip()
        if cols >= 3:
            comarca_name = line[cols - 3].strip()

        municipio_uid = line[cols - 2].strip()
        municipio_name = line[cols - 1].strip()

        provincias[provincia_name][comarca_name][municipio_uid] = municipio_name

    #        print(municipio_name,",", comarca_name, provincia_name)
    #    pprint(provincias)
    return provincias


import pytz
from datetime import datetime

local_tz = pytz.timezone("Europe/Madrid")

url = "https://tornallom.org/services/searchEntitiesGeojson/"
url_api = "https://tornallom.org/backoffice/api"
media_full_path = (
    "/home/alberto/workspace/otrosproyectos/tornallom/django/tornallom/media/"
)


def get_evaluations():
    import mapPointCriterias.evaluations_data as evaluations_data

    for point_slug, data in evaluations_data.EVALUATIONS.items():
        print("Loading criteria answers for point_slug", point_slug)
        point = MapPoint.objects.get(slug=point_slug)

        postal_code = data["postal_code"]
        editorname = data["editor"].lower()
        invoicing = data["anual_invoicing"]
        year_start = data["foundation_year"]
        if not editorname:
            editorname = "Tornallom"
        editorname = slugify(editorname).replace("-", "")

        user, created = User.objects.get_or_create(username=editorname)
        user.set_password("randomrandom23131231")
        user.save()
        point.editor = user
        point.postal_code = postal_code
        point.year_start = year_start
        point.invoicing = invoicing
        point.save()

        for role, ratios_str in data["ratios"].items():
            ratios = {"women": 0, "men": 0, "others": 0}

            ratios_array = ratios_str.split(",")
            if len(ratios_array) >= 1:
                if not ratios_array[0]:
                    ratios["women"] = 0
                else:
                    ratios["women"] = ratios_array[0]
            if len(ratios_array) >= 2:
                if not ratios_array[1]:
                    ratios["men"] = 0
                else:
                    ratios["men"] = ratios_array[1]
            if len(ratios_array) >= 3:
                if not ratios_array[2]:
                    ratios["others"] = 0
                else:
                    ratios["others"] = ratios_array[2]

            print("Getting role name", role)
            role_obj = ParticipantRole.objects.get(name=role)
            ParticipantRoleForMapPoint.objects.create(
                role=role_obj,
                mappoint=point,
                women=ratios["women"],
                men=ratios["men"],
                others=ratios["others"],
            )
        comments = data["comments"]
        for com in comments:
            if com["date"]:
                commenter = com["commenter"].lower()
                print("Adding Comment from", commenter, com["date"])
                author = User.objects.get(username=commenter)
                point.point_comments.create(
                    text=com["text"], author=author, published_date=com["date"]
                )

        for criteria_slug, c_data in data["criterias"].items():
            c_obj = Criteria.objects.get(slug=criteria_slug)
            notes = c_data["notas"]
            dont_apply = not c_data["no-aplica"] == ""
            points = {}
            for n in range(0, 6):
                value = c_data["p" + str(n + 1)]
                if value == "X":
                    points[n] = 1
                elif value == "x":
                    points[n] = 0.5
                else:
                    points[n] = 0

            EvaluationCriteriasForMapPoint.objects.create(
                criteria=c_obj,
                mappoint=point,
                notes=notes,
                dont_apply=dont_apply,
                points_0=points[0],
                points_1=points[1],
                points_2=points[2],
                points_3=points[3],
                points_4=points[4],
                points_5=points[5],
            )


def get_and_save_live_data_entities_oldbackend():

    MapPoint.objects.all().delete()

    entities_data = requests.post(url, json={"key": "value"})
    for identity in entities_data.json()["response"]:
        # print(identity)
        id = identity["properties"]["id"]
        response = requests.get(
            url_api + "/entities/{}/".format(id), json={"key": "value"}
        )
        d = response.json()["response"]
        print("-" * 30)
        print("slug", d["normalizedName"])
        # pprint(d)
        geopoint = Point(d["longitude"], d["latitude"], srid=4326)
        # geopoint = Point(d['longitude'], d['latitude'], srid=3857)

        status_name = d["entityStatus"]["name"]
        if status_name == "Publicat":
            status_name = "PUBLISHED"
        elif status_name == "Proposat":
            status_name = "DRAFT"
        print("status_name", status_name)
        status = status_name
        legal_form = (
            LegalForm.objects.get(name=d["legalForm"]["name"])
            if "legalForm" in d
            else None
        )
        province = LandLevel_1.objects.get(name=d["province"]["name"])
        county = LandLevel_2.objects.get(name=d["region"]["name"])
        town_name = d["town"]["name"]
        if town_name.startswith("Sant Vicent del Raspeig"):
            town_name = "Sant Vicent del Raspeig"
        elif town_name.startswith("Castelló de la Plana"):
            town_name = "Castelló de la Plana"
        town = LandLevel_3.objects.get(name=town_name)

        if "xesBalance" in d:
            if d["xesBalance"]:
                balance_social = TrinaryBoolean.YES
            else:
                balance_social = TrinaryBoolean.NO
        else:
            balance_social = TrinaryBoolean.NOT_APPLY

        # points profiles pictures
        download_point_pictures = False
        picture_path = "mappoint_pictures/" + d["normalizedName"] + ".jpg"
        if download_point_pictures:
            print("Downloading Point pictures", picture_path)
            image_url = url_api + d["pictureUrl"]
            r = requests.get(image_url, stream=True)
            if r.status_code == 200:
                with open(media_full_path + picture_path, "wb") as f:
                    for chunk in r:
                        f.write(chunk)

        main_category = Category.objects.get(name=d["mainSector"]["name"])
        if "sectorCcae" in d:
            burocracy_category = BurocracyCategory.objects.get(
                name=d["sectorCcae"]["name"]
            )
        else:
            burocracy_category = None

        timestamp_date = int(str(d["registryDate"])[0:10])
        published_date = datetime.utcfromtimestamp(timestamp_date).replace(
            tzinfo=pytz.utc
        )

        timestamp_date = int(str(d["entityEvaluation"]["date"])[0:10])
        evaluation_date = datetime.utcfromtimestamp(timestamp_date).replace(
            tzinfo=pytz.utc
        )

        params = {
            "name": d["name"].strip(),
            "description": d["description"].strip(),
            "location": geopoint,
            "burocracy_number": d["nif"],
            "status": status,
            "phone": d["phone"],
            "email": d["email"],
            "slug": d["normalizedName"],
            "legal_form": legal_form,
            "opening_hours": d["openingHours"],
            "landlevel_1": province,
            "landlevel_2": county,
            "landlevel_3": town,
            "address": d["address"],
            "contact_person": d["contactPerson"],
            "picture": picture_path,
            "main_category": main_category,
            "burocracy_category": burocracy_category,
            "social_approval": balance_social,
            "created_date": published_date,
            "evaluation_date": evaluation_date,
        }

        p = MapPoint.objects.create(**params)
        for s in d["sectors"]:
            other_sector = Category.objects.get(name=s["name"])
            p.other_categories.add(other_sector)

        if d["twitter"]:
            PointLink.objects.create(
                link_type=LinkType.TWITTER, url=d["twitter"], point=p
            )
        if d["facebook"]:
            PointLink.objects.create(
                link_type=LinkType.FACEBOOK, url=d["facebook"], point=p
            )
        if d["instagram"]:
            PointLink.objects.create(
                link_type=LinkType.INSTAGRAM, url=d["instagram"], point=p
            )
        if d["pinterest"]:
            PointLink.objects.create(
                link_type=LinkType.PINTEREST, url=d["pinterest"], point=p
            )
        if d["web"]:
            PointLink.objects.create(link_type=LinkType.WEBSITE, url=d["web"], point=p)

        print("description", len(p.description))
        print("trans_description", len(get_translation(p.description)))
        MapPointTranslation.objects.create(
            name=get_translation(p.name),
            description=get_translation(p.description),
            language="es",
            original=p,
        )

        # #Camps agafar manualment:
        # postal_code, que hi pots trobar, altres xarxes, editor, facturació anual, any fundació, Comentaris,
        # Avaluació(preguntes, notes int.)
        # persones remunerades/carrecs directius/organs de govern/persones no remunerades
        # Punts propossats

    return


class Command(BaseCommand):
    help = (
        "Generate fake data for local development and testing purposes"
        "and load it into the database"
    )

    def add_arguments(self, parser):
        parser.add_argument(
            "--delete",
            action="store_true",
            dest="delete",
            help="Delete previous highlights, homepage, landing page, milestones, news and people from the database",
        )

        parser.add_argument(
            "--seed",
            action="store",
            dest="seed",
            help="A seed value to pass to Faker before generating data",
        )

    def create_groups(self):

        models = ["mappoint", "comment", "pointlink", "mappointtranslation"]
        for m in models:
            content_type = ContentType.objects.get(
                app_label="mapPointCriterias", model=m
            )
            permission_add = Permission.objects.get(
                codename=f"add_{m}", content_type=content_type
            )
            permission_view = Permission.objects.get(
                codename=f"view_{m}", content_type=content_type
            )
            permission_change = Permission.objects.get(
                codename=f"change_{m}", content_type=content_type
            )
            permission_delete = Permission.objects.get(
                codename=f"delete_{m}", content_type=content_type
            )
            superaixadetes_group.permissions.add(
                permission_add, permission_view, permission_change, permission_delete
            )
            aixadetes_group.permissions.add(
                permission_add, permission_view, permission_change, permission_delete
            )

    def handle(self, *args, **options):

        if options["delete"]:
            call_command("flush_models")

        print("Flush all models")
        User.objects.all().delete()
        MapPoint.objects.all().delete()
        Category.objects.all().delete()
        CategoryTranslation.objects.all().delete()
        BurocracyCategory.objects.all().delete()
        # Language.objects.all().delete()
        LegalForm.objects.all().delete()
        ParticipantRole.objects.all().delete()
        Criteria.objects.all().delete()
        CriteriaTranslation.objects.all().delete()
        Comment.objects.all().delete()

        Network.objects.all().delete()
        # PointStatus.objects.all().delete()
        Community.objects.all().delete()
        LandLevel_1.objects.all().delete()
        LandLevel_2.objects.all().delete()
        LandLevel_3.objects.all().delete()

        self.create_groups()

        #
        # listingsportal_page =  MapListingIndexPage.objects.first()
        # faker_en = factory.faker.Faker._get_faker(locale='en-US')
        # faker_en.random.seed(seed)
        # faker_es = factory.faker.Faker._get_faker(locale='es-ES')
        # faker_es.random.seed(seed)
        # load from a json in the future

        # create demo superuser
        demo = User.objects.create(
            username="admin", is_staff=True, is_superuser=True, email="kapis@riseup.net"
        )
        demo.set_password("7yeonyie7yeonyie")
        demo.save()

        sorted = list(initial_data.BUROCRACY_CATEGORIES.keys())
        sorted.sort()
        for key in sorted:
            name = initial_data.BUROCRACY_CATEGORIES[key]
            BurocracyCategory.objects.create(codenumber=key, name=name)

        for parent, children in initial_data.CATEGORIES.items():
            slug = slugify(parent)
            parent_obj = Category.objects.create(
                name=parent,
                parent=None,
                icon=f"category_icons/{slug}.png",
                mapicon=f"category_mapicons/{slug}.png",
            )
            CategoryTranslation.objects.create(
                name=get_translation(parent), language="es", original=parent_obj
            )

            for child in children:
                slug = slugify(child)
                child_obj = Category.objects.create(
                    name=child,
                    parent=parent_obj,
                    icon=f"category_icons/{slug}.png",
                    mapicon=f"category_mapicons/{slug}.png",
                )
                CategoryTranslation.objects.create(
                    name=get_translation(child), language="es", original=child_obj
                )

        # Agafar iconos de https://tornallom.org/backoffice/api/sectors
        download_sector_icons = False

        if download_sector_icons:
            print("Downloading Sector icons")
            response = requests.get(
                url_api + "/sectors/".format(id), json={"key": "value"}
            )
            sectors = response.json()
            for c in sectors:

                c_obj = Category.objects.get(name=c["name"])
                slug_sector = slugify(c["name"])
                print("Downloading ", slug_sector)
                image_url = url_api + c["iconUrl"]
                icon_path = "category_icons/{}.png".format(slug_sector)
                r = requests.get(image_url, stream=True)
                if r.status_code == 200:
                    with open(media_full_path + icon_path, "wb") as f:
                        for chunk in r:
                            f.write(chunk)
                    c_obj.icon = icon_path
                    c_obj.save()

                image_url = url_api + c["mapIconUrl"]
                r = requests.get(image_url, stream=True)
                if r.status_code == 200:
                    mapicon_path = "category_mapicons/{}.png".format(slug_sector)
                    with open(media_full_path + mapicon_path, "wb") as f:
                        for chunk in r:
                            f.write(chunk)
                    c_obj.mapicon = mapicon_path
                    c_obj.save()

        # sorted = list(initial_data.LANGUAGES.keys())
        # sorted.sort()
        # for key in sorted:
        #     name = initial_data.LANGUAGES[key]
        #     Language.objects.create(i10n_code=key, name=name)

        sorted = list(initial_data.LEGAL_FORMS)
        sorted.sort()
        for name in sorted:
            LegalForm.objects.create(name=name)

        for order_num, (name, description) in enumerate(initial_data.PARTICIPANT_ROLES):
            ParticipantRole.objects.create(
                name=name, description=description, my_order=order_num
            )

        for username, email in initial_data.USERS.items():
            u, _ = User.objects.get_or_create(
                username=username, email=email, is_staff=True, is_active=True
            )
            u.set_password(username)
            u.save()
            u.groups.add(superaixadetes_group)
            u.save()
        # group_xemac = Group.objects.get(name="EditoresXEMAC")

        # sorted = list(initial_data.NETWORKS)
        # sorted.sort()
        # for name in sorted:
        Network.objects.create(name="Xarxa test")

        sorted = list(initial_data.COMMUNITIES)
        sorted.sort()
        for name in sorted:
            Community.objects.create(name=name)

        sorted = list()
        # for status in initial_data.STATUS:
        #     PointStatus.objects.create(
        #         codename=status["codename"], name=status["name"], type=status["type"]
        #     )

        # for status in initial_data.STATUS:
        #     if status["next"]:
        #         s = PointStatus.objects.get(codename=status["next"])
        #         PointStatus.objects.update(next=s)

        sorted = list()
        for order_num, data in enumerate(initial_data.CRITERIAS):
            parent_obj = Criteria.objects.create(
                name=data["title"],
                slug=slugify(data["title"]),
                description=data["description"],
                question=data["question"],
                answer_0=data["answer_1"],
                answer_1=data["answer_2"],
                answer_2=data["answer_3"],
                answer_3=data["answer_4"],
                answer_4=data["answer_5"],
                answer_5=data["answer_6"],
                my_order=order_num,
            )

            CriteriaTranslation.objects.create(
                name=get_translation(parent_obj.name),
                description=get_translation(parent_obj.description),
                language="es",
                original=parent_obj,
            )
            # print(slugify(data["title"]))

        download_criteria_icons = False
        response = requests.get(
            url_api + "/criteria/".format(id), json={"key": "value"}
        )
        criterias = response.json()["response"]
        for c in criterias:
            if c["name"] == "Proveïdors":
                name = "Proveïdores"
            else:
                name = c["name"]
            c_obj = Criteria.objects.get(name=name)
            image_url = url_api + c["iconUrl"]
            slug_criteria = slugify(name)
            picture_path = "criteria_icons/{}.png".format(slug_criteria)
            if download_criteria_icons:
                print("Downloading Criteria icons")
                r = requests.get(image_url, stream=True)
                if r.status_code == 200:
                    with open(media_full_path + picture_path, "wb") as f:
                        for chunk in r:
                            f.write(chunk)
            c_obj.icon = picture_path
            c_obj.save()

        # sorted = list()
        # for data in initial_data.MAPPOINTS:
        #     status = PointStatus.objects.get(name=data["status"])
        #     MapPoint.objects.create(name=data["name"],status=status)

        provinces = get_territories()
        for landlevel1_name, landlevel2_dict in provinces.items():
            landlevel1_obj, exists = LandLevel_1.objects.get_or_create(
                name=landlevel1_name
            )  # provinces
            for landlevel2_name, landlevel3_dict in landlevel2_dict.items():
                landlevel2_obj, exists = LandLevel_2.objects.get_or_create(
                    name=landlevel2_name, parent=landlevel1_obj
                )  # comarcas
                for landlevel3_uid, landlevel3_name in landlevel3_dict.items():
                    landlevel3_obj = LandLevel_3.objects.create(
                        name=landlevel3_name, parent=landlevel2_obj
                    )  # towns

        get_and_save_live_data_entities_oldbackend()
        get_evaluations()
        #
        # for phrase, trans in initial_data.TRANSLATIONS.items():
        #     print('"""', phrase, '"""', " : ", '"""', trans, '""",')

        print(self.style.SUCCESS("Done!"))
