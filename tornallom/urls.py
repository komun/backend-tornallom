"""tornallom URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from mapPointCriterias import views
from baton.autodiscover import admin
from django.urls import path, re_path, include
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.base import RedirectView

urlpatterns = [
    path("", RedirectView.as_view(url="/admin/")),
    path("accounts/", include("django_registration.backends.one_step.urls")),
    # path('accounts/', include('django_registration.backends.activation.urls')),
    path("accounts/", include("django.contrib.auth.urls")),
    path("admin/", admin.site.urls),
    path("baton/", include("baton.urls")),
    re_path(
        "api/(?P<endpoint>[\w-]+)/(?P<param>[\w-]+)/?$", views.api_endpoint_with_param
    ),
    re_path("api/(?P<endpoint>[\w-]+)/?$", views.api_endpoints),
]
urlpatterns += static("api/media/", document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import os.path

    # settings.STATICFILES_DIRS += os.path.join(settings.BASE_DIR, "frontend","static"),
    # print('settings.STATICFILES_DIRS', settings.STATICFILES_DIRS)
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    FRONTEND_URL = "/static/"
    FRONTEND_ROOT = os.path.join(settings.BASE_DIR, "mapPointCriterias", "static")
    print("FRONTEND_ROOT", FRONTEND_ROOT)
    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(FRONTEND_URL, document_root=FRONTEND_ROOT)
