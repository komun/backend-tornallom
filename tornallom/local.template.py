
DEBUG = True

EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
DEFAULT_FROM_EMAIL = "Tornallom.org <xxxxxxxxxxxx>"
DEFAULT_REPLYTO_EMAIL = "Tornallom.org <info@xxxxxx.org>"

EMAIL_HOST = 'xxxx.org'
EMAIL_HOST_USER = 'koxxxxxxsmo@xxxx.org'
EMAIL_HOST_PASSWORD = 'xxxxxxxxxxx'
EMAIL_PORT = 587
EMAIL_USE_TLS = True




SECRET_KEY = "DJANGO_SECRET_KEY_ADD_YOURS_HERE"
WAGTAILADMIN_NOTIFICATION_USE_HTML = True
ADMINS_MAILS = ['xxxx@xxxxx.net']

DATABASES = {
     'default': {
         'ENGINE': 'django.contrib.gis.db.backends.postgis',
         'NAME': 'xxxxxxxxxx',
         'USER': 'xxxxxxxxxx',
         'PASSWORD': 'xxxxxxxx',
         'HOST': 'localhost',
         'PORT': '5432'
     }
 }
